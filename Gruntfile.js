const execSync = require('child_process').execSync;


module.exports = (grunt) => {
	grunt.initConfig({});

	grunt.registerTask('build-minecraftshire', () => {
		execSync('node node_modules/webpack/bin/webpack --config ./tasks/minecraftshire.config.js', {stdio: 'inherit'});
	});

	grunt.registerTask('build-minecraftshire-minified', () => {
		execSync('node node_modules/webpack/bin/webpack --config ./tasks/minecraftshire-minified.config.js', {stdio: 'inherit'});
	});

	grunt.registerTask('build-rushacker', () => {
		execSync('node node_modules/webpack/bin/webpack --config ./tasks/rushacker.config.js', {stdio: 'inherit'});
	});

	grunt.registerTask('build-rushacker-minified', () => {
		execSync('node node_modules/webpack/bin/webpack --config ./tasks/rushacker-minified.config.js', {stdio: 'inherit'});
	});

	grunt.registerTask('build-storybook', () => {
		execSync('npm run build-storybook');
	});

	grunt.registerTask('build', [
		'build-minecraftshire', 'build-minecraftshire-minified',
		'build-rushacker', 'build-rushacker-minified',
		// 'build-storybook'
	]);
};
