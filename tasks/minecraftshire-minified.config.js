const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');


module.exports = {
	...require('./minecraftshire.config'),
	output: {
		path: path.resolve(__dirname, '../dist/minecraftshire/'),
		filename: 'index.min.js',
		sourceMapFilename: 'index.min.js.map',
		libraryTarget: 'amd'
	},
	plugins: [
		new UglifyJsPlugin()
	]
};
