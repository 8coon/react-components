const path = require('path');

module.exports = {
	...require('./minecraftshire.config'),
	output: {
		path: path.resolve(__dirname, '../dist/rushacker/'),
		filename: 'index.js',
		sourceMapFilename: 'index.min.js.map',
		libraryTarget: 'amd'
	},
	resolve: {
		extensions: ['.js', '.ts', '.tsx'],
		alias: {
			'../../themes/index': '../../themes/rushacker/index'
		}
	}
};
