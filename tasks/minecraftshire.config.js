const path = require('path');


module.exports = {
	entry: './src/index.ts',
	output: {
		path: path.resolve(__dirname, '../dist/minecraftshire/'),
		filename: 'index.js',
		sourceMapFilename: 'index.js.map',
		libraryTarget: 'amd'
	},
	resolve: {
		extensions: ['.js', '.ts', '.tsx'],
		alias: {
			'../../themes/index': '../../themes/minecraftshire/index'
		}
	},
	devtool: 'source-map',
	target: 'web',
	module: {
		rules: [
			{
				test: /\.ts[x]?$/,
				exclude: /(node_modules|storybook-static|dist)/,
				loader: 'ts-loader'
			},
			{
				test: /\.js[x]?$/,
				exclude: /(node_modules|storybook-static|dist)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['preset-react'],
					}
				}
			}
		]
	},
	externals: ['react', 'react-dom', 'aphrodite', 'aphrodite/no-important']
};
