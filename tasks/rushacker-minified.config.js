const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');


module.exports = {
	...require('./rushacker.config'),
	output: {
		path: path.resolve(__dirname, '../dist/rushacker/'),
		filename: 'index.min.js',
		sourceMapFilename: 'index.min.js.map',
		libraryTarget: 'amd'
	},
	plugins: [
		new UglifyJsPlugin()
	]
};
