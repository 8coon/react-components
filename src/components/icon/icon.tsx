import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import ThemeStyleSheet from '../../themes/index';

export interface SVGNode {
	name: string,
	attrs?: object,
	childs?: SVGNode[]
}

export interface UIIconProps {
	icon: SVGNode,
	width?: number,
	height?: number,
	fill?: string
}

const createTag = (node: SVGNode) => React.createElement(
		node.name,
		Object
			.keys(node.attrs)
			.reduce((props, prevKey) => {
				let nextKey = prevKey;

				if (prevKey === 'viewportFill') {
					nextKey = prevKey.toLowerCase();
				}

				props[nextKey] = node.attrs[prevKey];
				return props;
			}, {}),
		...(node.childs ? node.childs.map(child => createTag(child)) : [])
	);

const UIIconStylesheet = StyleSheet.create({
	'default': {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center'
	}
});

const UIIcon: React.StatelessComponent<UIIconProps> = props => {
	const icon = props.icon;

	props.width && (icon.attrs['width'] = props.width);
	props.height && (icon.attrs['height'] = props.height);
	props.fill && (icon.attrs['fill'] = props.fill);

	return (
		<span className={css(
			UIIconStylesheet.default,
			ThemeStyleSheet.UIIcon.default
		)}>
		{createTag(props.icon)}
	</span>
	);
};

export default UIIcon;
