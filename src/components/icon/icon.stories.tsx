import * as React from 'react';

import UIIcon from './icon';
import Icon from 'svgson-loader!./icon.bench.svg';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

declare const module: any;

storiesOf('UIIcon', module)

	.add('Иконка', () =>
		<div style={{
			backgroundColor: '#333',
			padding: 10,
			height: 100,
			width: 100
		}}>
			<UIIcon icon={Icon}/>
		</div>);
