import * as React from 'react';

import UIText from "./text";

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

declare const module: any;

export const loremIpsum = (
	<div>
		<UIText>
			Scriggins! Bite my entire ass! Go legally brain dead from lack of oxygen due to choking
			on my entire ass!
		</UIText>

		<UIText>
			&mdash; Eels honey. Eeeeeels
		</UIText>

		<UIText>
			&mdash; Eeeeeeeeels, mom.
		</UIText>

		<UIText>
			Mae is a cat with dark blue fur and dyed red hair, which has faded since she applied it and now
			appears to be dark red in color. She has large red eyes, which she dislikes and refers to as
			"nightmare eyes." Her right ear is also notched from a dog attack, and Mae can be seen flicking
			this ear frequently throughout the game while idle. Her attire consists of an orange shirt with
			long red sleeves. It has a slashed zero design in the center, which can be a symbol for the
			"non-binary" gender, but was called a "coincidence" by Scott Benson. Mae also has dark brown jeans
			and wears boots.
		</UIText>
	</div>
);

storiesOf('UIText', module)

	.add('Текст', () =>
		<div>{loremIpsum}</div>);
