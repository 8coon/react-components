import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import ThemeStyleSheet from '../../themes/index';

export interface UITextProps {
	bare?: boolean;
}

const UITextStyle = StyleSheet.create({
	'default': {
		display: 'block'
	},

	'bare': {}
});

const UIText: React.StatelessComponent<UITextProps> = props => {
	return (
		<span className={css(
			UITextStyle.default,
			ThemeStyleSheet.UIText.default,
			props.bare && UITextStyle.bare,
			props.bare && ThemeStyleSheet.UIText.bare,
		)}>
			{props.children}
		</span>
	)
};

export default UIText;
