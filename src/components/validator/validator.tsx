import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import {SVGNode, default as UIIcon} from '../icon/icon';
import {ReactElement, SyntheticEvent} from 'react';
import UIVerticalLayout from '../../layouts/vertical/vertical'

import ThemeStyleSheet from '../../themes/index';

export interface UIValidatorProps {
	icon?: SVGNode;
	text?: string;
	grow?: boolean;
	bare?: boolean;
	validated?: boolean;
	justify?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around';
	align?: 'stretch' | 'center' | 'flex-start' | 'flex-end' | 'baseline';
	onValidate: (...values: string[]) => void;
}

const UIValidatorStyle = StyleSheet.create({
	'default': {
		display: 'inline-block'
	},

	'grow': {
		width: '100%'
	},

	'errorBlock': {
		display: 'flex',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center'
	},

	'errorBlockEmpty': {
		height: 0
	},

	'leftIcon': {
	}
});

const UIValidator: React.StatelessComponent<UIValidatorProps> = props => {
	const hasError = props.icon || props.text;
	const values = [];

	React.Children
		.forEach(props.children, child => {
			if (!React.isValidElement(child)) {
				return;
			}

			values.push(child.props['text']);
		});

	const onChildChange = () => {
		props.onValidate(...values);
	};

	if (props.validated === false) {
		onChildChange();
	}

	const isGrow = props.grow;
	const noError = !hasError;

	return (
		<span className={css(
			UIValidatorStyle.default,
			ThemeStyleSheet.UIValidator.default,
			isGrow && UIValidatorStyle.grow,
			isGrow && ThemeStyleSheet.UIValidator.grow
		)}>
			<UIVerticalLayout bare={props.bare}
							  justify={props.justify}
							  align={props.align}>
				{React.Children.map(props.children, (child: ReactElement<any>, i) => {
					if (!React.isValidElement(child)) {
						return {child}
					}

					const onChange = child.props['onChange'];

					return React.cloneElement(child as any, {
						key: i,
						grow: child.props['grow'] === void 0 ? props.grow : child.props['grow'],
						hasError: child.props['hasError'] === void 0 ? hasError : child.props['hasError'],
						onChange: (event: SyntheticEvent<any>, ...args: any[]) => {
							onChange && onChange(event, ...args);
							values[i] = event.target['value'];
							onChildChange();
						}
					});
				})}

				<span className={css(
					UIValidatorStyle.errorBlock,
					ThemeStyleSheet.UIValidator.errorBlock,
					noError && UIValidatorStyle.errorBlockEmpty,
					noError && ThemeStyleSheet.UIValidator.errorBlockEmpty
				)}>
				{
					props.icon && <span className={css(
						UIValidatorStyle.leftIcon,
						ThemeStyleSheet.UIValidator.leftIcon
					)}>
						<UIIcon icon={props.icon}/>
					</span>
				}
					{
						props.text && <span>{props.text}</span>
					}
			</span>
			</UIVerticalLayout>
		</span>
	);
};

export default UIValidator;
