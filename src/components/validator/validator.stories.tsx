import * as React from 'react';

import UIInput from '../input/input';
import UIValidator from './validator';
import Icon from 'svgson-loader!./../icon/icon-dark.bench.svg';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

declare const module: any;

storiesOf('UIValidator', module)

	.add('Без ошибок', () =>
		<UIValidator onValidate={action('onValidate')}>
			<UIInput text={'lol'}/>
			<UIInput text={'kek'}/>
		</UIValidator>)

	.add('Простая ошибка', () =>
		<UIValidator onValidate={action('onValidate')}
					 text={'Ты просто неправ'}>
			<UIInput text={'lol'}/>
			<UIInput text={'kek'}/>
		</UIValidator>)

	.add('Ошибка с иконкой', () =>
		<UIValidator onValidate={action('onValidate')}
					 icon={Icon}
					 text={'Ты неправ, и вот почему'}>
			<UIInput text={'lol'}/>
			<UIInput text={'kek'}/>
		</UIValidator>);
