import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import ThemeStyleSheet from '../../themes/index';

export interface UIWindowProps {
	grow?: boolean
}

const UIWindowStyle = StyleSheet.create({
	'default': {
		display: 'block'
	},

	'grow': {
		flex: 1
	}
});

const UIWindow: React.StatelessComponent<UIWindowProps> = props => {
	return (
		<div className={css(
			UIWindowStyle.default,
			ThemeStyleSheet.UIWindow.default,
			props.grow && UIWindowStyle.grow,
			props.grow && ThemeStyleSheet.UIWindow.grow
		)}>
			{props.children}
		</div>
	)
};

export default UIWindow;
