import * as React from 'react';

import UIWindow from './window';
import UICurtain from '../curtain/curtain';
import UIHeadline from '../headline/headline';
import UIButton from '../button/button';
import UIText from '../text/text';
import {loremIpsum} from '../text/text.stories';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

declare const module: any;

storiesOf('UIWindow', module)

	.add('Всплывающее окно', () =>
		<UICurtain>
			<UIWindow>
				<UIHeadline>
					Всплывающее окно
				</UIHeadline>

				<UIText>
					{loremIpsum}
				</UIText>
			</UIWindow>
		</UICurtain>)

	.add('Всплывающее окно во всю ширину', () =>
		<UICurtain>
			<UIWindow grow>
				It Works!
			</UIWindow>
		</UICurtain>);
