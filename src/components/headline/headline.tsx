import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import ThemeStyleSheet from '../../themes/index';

export interface UIHeadlineProps {
	size?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5'
}

const UIHeadlineStyle = StyleSheet.create({
});

const UIHeadline: React.StatelessComponent<UIHeadlineProps> = props => {
	const size = props.size || 'h1';

	return (
		<span className={css(
			ThemeStyleSheet.UIHeadline[size]
		)}>
			{props.children}
		</span>
	)
};

export default UIHeadline;
