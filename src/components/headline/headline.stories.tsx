import * as React from 'react';

import UIHeadline from './headline';
import UIText from '../text/text';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

declare const module: any;

storiesOf('UIHeadline', module)

	.add('H1 Заголовок', () =>
		<div>
			<UIHeadline size="h1">
				Заголовок!
			</UIHeadline>

			<UIText>
				Какой-то текст дальше
			</UIText>
		</div>)

	.add('H2 Заголовок', () =>
		<div>
			<UIHeadline size="h2">
				Заголовок!
			</UIHeadline>

			<UIText>
				Какой-то текст дальше
			</UIText>
		</div>)

	.add('H3 Заголовок', () =>
		<div>
			<UIHeadline size="h3">
				Заголовок!
			</UIHeadline>

			<UIText>
				Какой-то текст дальше
			</UIText>
		</div>)

	.add('H4 Заголовок', () =>
		<div>
			<UIHeadline size="h4">
				Заголовок!
			</UIHeadline>

			<UIText>
				Какой-то текст дальше
			</UIText>
		</div>)

	.add('H5 Заголовок', () =>
		<div>
			<UIHeadline size="h5">
				Заголовок!
			</UIHeadline>

			<UIText>
				Какой-то текст дальше
			</UIText>
		</div>);
