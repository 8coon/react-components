import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import ThemeStyleSheet from '../../themes/index';

export interface UIHrProps {
}

const UIHr: React.StatelessComponent<UIHrProps> = props => {
	return (
		<div className={css(
			ThemeStyleSheet.UIHr.default
		)}/>
	);
};

export default UIHr;
