import * as React from 'react';

import UIForm from './form';

import UIButton from '../button/button';
import UIInput from '../input/input';
import UIValidator from '../validator/validator';
import UIVerticalLayout from '../../layouts/vertical/vertical';
import UIHorizontalLayout from '../../layouts/horizontal/horizontal';
import UIGroup from '../group/group';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

declare const module: any;


class Form extends React.PureComponent<{grow?: boolean, pure?: boolean, error?: string, login?: string, password?: string}> {
	render() {
		return (
			<UIForm onSubmit={action('onSubmit')}
					grow={this.props.grow}
					pure={this.props.pure}>
				<UIVerticalLayout>

					<UIValidator onValidate={action('onValidate')}
								 grow
								 text={this.props.error}>
						<UIInput placeholder="Логин"
								 text={this.props.login}
								 grow/>
						<UIInput placeholder="Пароль"
								 text={this.props.password}
								 password
								 grow/>
					</UIValidator>

					<UIGroup grow
							 pure
							 bare>
						<UIHorizontalLayout justify="flex-end">
							<UIButton text="Регистрация"
									  grow
									  onClick={action('onSignUp')}/>

							<UIButton highlighted
									  text="Войти"
									  grow
									  onClick={action('onSubmit')}/>
						</UIHorizontalLayout>
					</UIGroup>

				</UIVerticalLayout>
			</UIForm>);
	}
}


storiesOf('UIForm', module)

	.add('Форма', () =>
		<Form pure/>)

	.add('Форма с рамкой', () =>
		<Form/>)

	.add('Форма во всю ширину', () =>
		<Form grow pure/>)

	.add('Форма с ошибками', () =>
		<Form pure
			  login="mae.borowski@gmail.com"
			  password="gregg_rules"
			  error="Неправильный логин или пароль"/>)

	.add('Форма с ошибками во всю ширину', () =>
		<Form grow pure
			  login="mae.borowski@gmail.com"
			  password="gregg_rules"
			  error="Неправильный логин или пароль"/>);
