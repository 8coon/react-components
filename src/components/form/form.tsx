import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import UIGroup from '../group/group';
import UIVerticalLayout from '../../layouts/vertical/vertical';

import ThemeStyleSheet from '../../themes/index';

export type onSubmitHandler = (event) => void;

export interface UIFormProps {
	onSubmit?: onSubmitHandler,
	grow?: boolean,
	pure?: boolean
}

const UIFormStyle = StyleSheet.create({
	'default': {
	}
});

const UIForm: React.StatelessComponent<UIFormProps> = props => {
	const onSubmit = event => {
		event && event.preventDefault();
		props.onSubmit && props.onSubmit(event);
	};

	return (
		<UIGroup grow={props.grow}
				 pure={props.pure}>
			<UIVerticalLayout bare>
				<form onSubmit={onSubmit}
					  className={css(
					UIFormStyle.default,
					ThemeStyleSheet.UIText.default
				)}>
					{props.children}
					<button type="submit"
							style={{display: 'none'}}>Send</button>
				</form>
			</UIVerticalLayout>
		</UIGroup>
	)
};

export default UIForm;
