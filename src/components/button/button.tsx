import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import UIIcon, {SVGNode} from '../icon/icon';

import ThemeStyleSheet from '../../themes/index';

export interface UIButtonProps {
	text?: string,
	icon?: SVGNode,
	leftIcon?: SVGNode,
	rightIcon?: SVGNode,
	disabled?: boolean,
	highlighted?: boolean,
	grow?: boolean,
	link?: boolean,
	href?: string,
	target?: string,
	pure?: boolean,
	short?: boolean,
	leftIconHeight?: number,
	leftIconWidth?: number,
	leftIconFill?: string,
	rightIconHeight?: number,
	rightIconWidth?: number,
	rightIconFill?: string,
	iconHeight?: number,
	iconWidth?: number,
	iconFill?: string,
	onClick?: (event: React.SyntheticEvent<any>) => void
}

const UIButtonStyle = StyleSheet.create({
	'default': {
		cursor: 'pointer',
		userSelect: 'none',
		display: 'inline-flex',
		boxSizing: 'border-box',
		alignItems: 'center',
		justifyContent: 'center',
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		whiteSpace: 'nowrap',
		textDecoration: 'none'
	},

	'disabled': {
		cursor: 'default'
	},

	'grow': {
		width: '100%'
	},

	'link': {
		display: 'inline',
		userSelect: 'auto',

		':hover': {
			textDecoration: 'underline'
		}
	}
});

const UIButton: React.StatelessComponent<UIButtonProps> = _p => {
	const props = {..._p};

	props.leftIcon = props.leftIcon || props.icon;
	props.leftIconHeight = props.leftIconHeight || props.iconHeight;
	props.leftIconWidth = props.leftIconWidth || props.iconWidth;
	props.leftIconFill = props.leftIconFill || props.iconFill;

	return (
		<a className={css(
			UIButtonStyle.default,
			ThemeStyleSheet.UIButton.default,
			props.disabled && UIButtonStyle.disabled,
			props.disabled && ThemeStyleSheet.UIButton.disabled,
			props.highlighted && !props.disabled && UIButtonStyle.highlighted,
			props.highlighted && !props.disabled && ThemeStyleSheet.UIButton.highlighted,
			props.grow && UIButtonStyle.grow,
			props.grow && ThemeStyleSheet.UIButton.grow,
			props.link && UIButtonStyle.link,
			props.link && ThemeStyleSheet.UIButton.link,
			props.pure && UIButtonStyle.pure,
			props.pure && ThemeStyleSheet.UIButton.pure,
			props.short && UIButtonStyle.short,
			props.short && ThemeStyleSheet.UIButton.short
		)} onClick={props.onClick}
		   href={props.href}
		   target={props.target}>
			{props.leftIcon && (
				<span className={css(
					UIButtonStyle.leftIcon,
					ThemeStyleSheet.UIButton.leftIcon,
					props.short && UIButtonStyle.shortIcon,
					props.short && ThemeStyleSheet.UIButton.shortIcon
				)}>
					<UIIcon icon={props.leftIcon}
							height={props.leftIconHeight}
							width={props.leftIconWidth}
							fill={props.leftIconFill}/>
				</span>
			)}
			{!props.short && props.text}
			{props.rightIcon && (
				<span className={css(
					UIButtonStyle.rightIcon,
					ThemeStyleSheet.UIButton.rightIcon,
					props.short && UIButtonStyle.shortIcon,
					props.short && ThemeStyleSheet.UIButton.shortIcon
				)}>
					<UIIcon icon={props.rightIcon}
							height={props.rightIconHeight}
							width={props.rightIconWidth}
							fill={props.rightIconFill}/>
				</span>
			)}
		</a>
	);
};

export default UIButton;
