import * as React from 'react';

import UIButton from './button';
import Icon from 'svgson-loader!./../icon/icon.bench.svg';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

declare const module: any;

storiesOf('UIButton', module)

	.add('Обычная кнопка', () =>
		<UIButton text="Button"
				  onClick={action('onClick')}/>)

	.add('Выключенная кнопка', () =>
		<UIButton text="Button"
				  disabled
				  onClick={action('onClick')}/>)

	.add('Подсвеченная кнопка', () =>
		<UIButton text="Button"
				  highlighted
				  onClick={action('onClick')}/>)

	.add('Кнопка с картинкой', () =>
		<UIButton text="Button"
				  icon={Icon}
				  onClick={action('onClick')}/>)

	.add('Кнопка с картинкой справа', () =>
		<UIButton text="Button"
				  rightIcon={Icon}
				  onClick={action('onClick')}/>)

	.add('Кнопка с двумя картинками', () =>
		<UIButton text="Button"
				  leftIcon={Icon}
				  rightIcon={Icon}
				  onClick={action('onClick')}/>)

	.add('Кнопка без текста', () =>
		<UIButton short
				  icon={Icon}
				  onClick={action('onClick')}/>)

	.add('Прозрачная кнопка', () =>
		<UIButton pure
				  text="Button"
				  icon={Icon}
				  onClick={action('onClick')}/>)

	.add('Прозрачная кнопка без текста', () =>
		<UIButton pure
				  short
				  icon={Icon}
				  onClick={action('onClick')}/>)

	.add('Кнопка во всю ширину', () =>
		<UIButton text="Button"
				  grow
				  onClick={action('onClick')}/>)

	.add('Ссылка', () =>
		<UIButton text="Google"
				  link
				  href="https://google.ru"
				  target="blank"/>);