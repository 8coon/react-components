import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import {SVGNode, default as UIIcon} from '../icon/icon';

import ThemeStyleSheet from '../../themes/index';

export interface UIInputProps {
	text?: string;
	placeholder?: string;
	password?: boolean;
	grow?: boolean;
	noAutoComplete?: boolean;
	disabled?: boolean;
	hasError?: boolean;
	leftIcon?: SVGNode;
	rightIcon?: SVGNode;
	icon?: SVGNode;
	autoFocus?: boolean;
	tabIndex?: number;
	onChange?: (event: React.SyntheticEvent<any>, value: string) => void;
}

const UIInputStyle = StyleSheet.create({
	'default': {
		outline: 0,
		borderStyle: 'none',
		borderWidth: 0,
		boxSizing: 'border-box',
		width: '100%'
	},

	'disabled': {
		cursor: 'default'
	},

	'wrapper_default': {
		borderStyle: 'solid',
		borderWidth: 1,
		boxSizing: 'border-box',
		display: 'inline-flex',
		alignItems: 'center',
		justifyContent: 'center'
	},

	'wrapper_disabled': {
		cursor: 'default'
	},

	'wrapper_grow': {
		width: '100%'
	}
});

const UIInput: React.StatelessComponent<UIInputProps> = _p => {
	const props = {..._p};
	props.leftIcon = props.leftIcon || props.icon;

	const inputAttrs = {};

	if (props.noAutoComplete) {
		inputAttrs['autocomplete'] = 'nope';
	}

	if (props.disabled) {
		inputAttrs['disabled'] = true;
	}

	const isGrow = props.grow;
	const isDisabled = props.disabled;
	const hasError = props.hasError;

	return (
		<span className={css(
					UIInputStyle.wrapper_default,
					ThemeStyleSheet.UIInput.wrapper_default,
					isGrow && UIInputStyle.wrapper_grow,
					isGrow && ThemeStyleSheet.UIInput.wrapper_grow,
					isDisabled && UIInputStyle.wrapper_disabled,
					isDisabled && ThemeStyleSheet.UIInput.wrapper_disabled,
					hasError && UIInputStyle.wrapper_hasError,
					hasError && ThemeStyleSheet.UIInput.wrapper_hasError
				)}>
			{props.leftIcon && (
				<span className={css(
					UIInputStyle.wrapper_leftIcon,
					ThemeStyleSheet.UIInput.wrapper_leftIcon
				)}>
					<UIIcon icon={props.leftIcon}/>
				</span>
			)}
			<input value={props.text || ''}
				   {...inputAttrs}
				   onChange={event => props.onChange && props.onChange(event, event.target.value)}
				   placeholder={props.placeholder}
				   tabIndex={props.tabIndex !== void 0 ? props.tabIndex : -1}
				   autoFocus={props.autoFocus}
				   {...(props.password ? {type: 'password'} : {})}
				   className={css(
						UIInputStyle.default,
						ThemeStyleSheet.UIInput.default,
						props.disabled && UIInputStyle.disabled,
						props.disabled && ThemeStyleSheet.UIInput.disabled
			)}/>
			{props.rightIcon && (
				<span className={css(
					UIInputStyle.wrapper_rightIcon,
					ThemeStyleSheet.UIInput.wrapper_rightIcon
				)}>
					<UIIcon icon={props.rightIcon}/>
				</span>
			)}
		</span>
	);
};

export default UIInput;
