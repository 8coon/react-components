import * as React from 'react';

import UIInput from './input';
import Icon from 'svgson-loader!./../icon/icon-dark.bench.svg';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

declare const module: any;

storiesOf('UIInput', module)

	.add('Обычный input', () =>
		<UIInput text=""
				 onChange={action('onChange')}/>)

	.add('Широкий input', () =>
		<UIInput text=""
				 grow
				 onChange={action('onChange')}/>)

	.add('Неактивный input', () =>
		<UIInput text=""
				 disabled
				 onChange={action('onChange')}/>)

	.add('Input с иконкой слева', () =>
		<UIInput text=""
				 icon={Icon}
				 onChange={action('onChange')}/>)

	.add('Input с иконкой справа', () =>
		<UIInput text=""
				 rightIcon={Icon}
				 onChange={action('onChange')}/>)

	.add('Input с двумя иконками', () =>
		<UIInput text=""
				 leftIcon={Icon}
				 rightIcon={Icon}
				 onChange={action('onChange')}/>)

	.add('Input с ошибкой', () =>
		<UIInput text="Что-то пошло не так"
				 hasError
				 onChange={action('onChange')}/>);
