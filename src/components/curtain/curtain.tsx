import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import ThemeStyleSheet from '../../themes/index';

export interface UICurtainProps {
	zIndex?: number,
}

const UICurtainStyle = StyleSheet.create({
	'default': {
		boxSizing: 'border-box',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		position: 'fixed',
		overflow: 'auto',
		height: '100%',
		width: '100%',
		left: 0,
		top: 0
	}
});

const UICurtain: React.StatelessComponent<UICurtainProps> = props => {
	const zIndex = props.zIndex || 0;

	return (
		<div className={css(
			UICurtainStyle.default,
			ThemeStyleSheet.UICurtain.default
		)} style={{zIndex}}>
			{props.children}
		</div>
	)
};

export default UICurtain;
