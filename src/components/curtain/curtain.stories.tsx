import * as React from 'react';

import UICurtain from "./curtain";

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

declare const module: any;

storiesOf('UICurtain', module)

	.add('Затенение', () =>
		<UICurtain zIndex={5}>
			<div style={{backgroundColor: 'white'}}>
				It Works!
			</div>
		</UICurtain>)

	.add('Затенение со скроллом', () =>
		<UICurtain zIndex={5}>
			<div style={{backgroundColor: 'white', minHeight: 2000}}>
				It Works!
			</div>
		</UICurtain>);
