import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import ThemeStyleSheet from '../../themes/index';

export interface UIGroupProps {
	grow?: boolean;
	text?: string;
	pure?: boolean;
	bare?: boolean;
	square?: boolean;
	verticalGrow?: boolean;
	justify?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around';
	align?: 'stretch' | 'center' | 'flex-start' | 'flex-end' | 'baseline';
	border?: 'none' | 'single' | 'double' | 'triple' | 'quadruple';
}

const UIGroupStyle = StyleSheet.create({
	'default': {
		boxSizing: 'border-box',
		display: 'inline-flex'
	},

	'grow': {
		width: '100%'
	},

	'borderNone': {
	},

	'borderSingle': {
	},

	'borderDouble': {
	},

	'borderTriple': {
	},

	'borderQuadruple': {
	},

	'square': {
	},

	'title': {
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		whiteSpace: 'nowrap'
	},

	'verticalGrow': {
		display: 'flex',
		flex: 1
	}
});

const UIGroup: React.StatelessComponent<UIGroupProps> = props => {
	const justify = props.justify || 'center';
	const align = props.align || 'flex-start';
	const border = props.bare ? 'none' : props.border || 'single';

	const borderNone = border === 'none';
	const borderSingle = border === 'single';
	const borderDouble = border === 'double';
	const borderTriple = border === 'triple';
	const borderQuadruple = border === 'quadruple';

	return (
		<span className={css(
			UIGroupStyle.default,
			ThemeStyleSheet.UIGroup.default,
			props.grow && UIGroupStyle.grow,
			props.grow && ThemeStyleSheet.UIGroup.grow,
			props.text && UIGroupStyle.hasTitle,
			props.text && ThemeStyleSheet.UIGroup.hasTitle,
			props.pure && UIGroupStyle.pure,
			props.pure && ThemeStyleSheet.UIGroup.pure,
			props.verticalGrow && UIGroupStyle.verticalGrow,
			props.verticalGrow && ThemeStyleSheet.UIGroup.verticalGrow,
			borderNone && UIGroupStyle.borderNone,
			borderNone && ThemeStyleSheet.UIGroup.borderNone,
			borderSingle && UIGroupStyle.borderSingle,
			borderSingle && ThemeStyleSheet.UIGroup.borderSingle,
			borderDouble && UIGroupStyle.borderDouble,
			borderDouble && ThemeStyleSheet.UIGroup.borderDouble,
			borderTriple && UIGroupStyle.borderTriple,
			borderTriple && ThemeStyleSheet.UIGroup.borderTriple,
			borderQuadruple && UIGroupStyle.borderQuadruple,
			borderQuadruple && ThemeStyleSheet.UIGroup.borderQuadruple,
			props.square && UIGroupStyle.square,
			props.square && ThemeStyleSheet.UIGroup.square,
		)} style={{
			alignItems: align,
			justifyContent: justify
		}}>
			{
				props.text && (
					<span className={css(
						UIGroupStyle.title,
						ThemeStyleSheet.UIGroup.title
					)}>
						{props.text}
					</span>
				)
			}
			{props.children}
		</span>
	);
};

export default UIGroup;
