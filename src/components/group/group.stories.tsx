import * as React from 'react';

import UIVerticalLayout from "../../layouts/vertical/vertical";
import UIGroup from "./group";
import UIButton from "../button/button";
import UIHorizontalLayout from "../../layouts/horizontal/horizontal";

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

declare const module: any;

storiesOf('UIGroup', module)

	.add('Обычная группа', () =>
		<UIGroup>
			<UIButton text="Кнопка"
					  grow/>
		</UIGroup>)

	.add('Во всю ширину', () =>
		<UIGroup grow>
			<UIButton text="Кнопка"
					  grow/>
		</UIGroup>)

	.add('С именем', () =>
		<UIGroup text="Группа с именем">
			<UIVerticalLayout>
				<UIHorizontalLayout>
					<UIButton text="Кнопка"/>
					<UIButton text="Кнопка"/>
					<UIButton text="Кнопка"/>
				</UIHorizontalLayout>
				<UIHorizontalLayout>
					<UIButton text="Кнопка"/>
					<UIButton text="Кнопка"/>
					<UIButton text="Кнопка"/>
				</UIHorizontalLayout>
				<UIHorizontalLayout>
					<UIButton text="Кнопка"/>
					<UIButton text="Кнопка"/>
					<UIButton text="Кнопка"/>
				</UIHorizontalLayout>
			</UIVerticalLayout>
		</UIGroup>)

	.add('Во всю ширину с именем', () =>
		<UIGroup text="Широкая группа с именем"
				 grow>
			<UIButton text="Кнопка"
					  grow/>
		</UIGroup>);
