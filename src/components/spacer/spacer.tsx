import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import ThemeStyleSheet from '../../themes/index';

export interface UISpacerProps {
	size?: 'none' | 'single' | 'double' | 'triple' | 'quadruple';
}

const UISpacer: React.StatelessComponent<UISpacerProps> = props => {
	const size = props.size || 'single';

	const none = size === 'none';
	const single = size === 'single';
	const double = size === 'double';
	const triple = size === 'triple';
	const quadruple = size === 'quadruple';

	return (
		<div className={css(
			none && ThemeStyleSheet.UISpacer.none,
			single && ThemeStyleSheet.UISpacer.single,
			double && ThemeStyleSheet.UISpacer.double,
			triple && ThemeStyleSheet.UISpacer.triple,
			quadruple && ThemeStyleSheet.UISpacer.quadruple,
		)}/>
	);
};

export default UISpacer;
