import {StyleSheet} from 'aphrodite/no-important';

import sizes from '../vars/sizes';
import {defaultFont} from '../vars/fonts';
import colors from '../vars/colors';
import {flicker} from '../vars/animations';

export default StyleSheet.create({
	'default': {
		...defaultFont,
		color: colors.white,
		backgroundColor: colors.orange,
		borderRadius: sizes._0,
		paddingTop: .5 * sizes._1,
		paddingBottom: .5 * sizes._1,
		paddingLeft: sizes._2,
		paddingRight: sizes._2,

		':hover': {
			backgroundColor: colors.orangeHigh
		},

		':active': {
			backgroundColor: colors.orangeLow
		}
	},

	'disabled': {
		backgroundColor: colors.grayHigh,

		':hover': {
			backgroundColor: colors.grayHigh
		},

		':active': {
			backgroundColor: colors.grayHigh
		}
	},

	'highlighted': {
		animationName: [flicker.func('boxShadow')(colors.orangeHigh)],
		animationDuration: flicker.duration,
		animationIterationCount: 'infinite',
		backgroundColor: colors.orangeLow,

		':hover': {
			backgroundColor: colors.orange
		},

		':active': {
			backgroundColor: colors.orangeLow
		}
	},

	'leftIcon': {
		paddingRight: sizes._1
	},

	'rightIcon': {
		paddingLeft: sizes._1
	}
});
