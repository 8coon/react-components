import {StyleSheet} from 'aphrodite/no-important';

import colors from '../vars/colors';
import sizes from '../vars/sizes';

export default StyleSheet.create({
	'default': {
		borderRadius: sizes._1,
		boxShadow: `0 0 20px -2px ${colors.transparentBlack}`,
		backgroundColor: colors.white,
		padding: sizes._3,
		width: 420
	},

	'grow': {
		borderRadius: 0,
		boxShadow: 'none'
	}
});
