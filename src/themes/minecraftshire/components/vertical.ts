import {StyleSheet} from 'aphrodite/no-important';
import sizes from '../vars/sizes';

export default StyleSheet.create({
	'child_padded': {
		paddingTop: 0.5 * sizes._2,
		paddingBottom: 0.5 * sizes._2
	}
});
