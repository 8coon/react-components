import {StyleSheet} from 'aphrodite/no-important';

import sizes from '../vars/sizes';

export default StyleSheet.create({
	'none': {
		minWidth: 0,
		minHeight: 0
	}
});
