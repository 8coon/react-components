import {StyleSheet} from 'aphrodite/no-important';
import sizes from '../vars/sizes';

export default StyleSheet.create({
	'child_padded': {
		paddingLeft: 0.5 * sizes._2,
		paddingRight: 0.5 * sizes._2
	}
});
