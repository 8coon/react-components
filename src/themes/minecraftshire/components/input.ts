import {StyleSheet} from 'aphrodite/no-important';

import sizes from '../vars/sizes';
import {defaultFont} from '../vars/fonts';
import colors from '../vars/colors';

export default StyleSheet.create({
	'default': {
		...defaultFont,
		lineHeight: sizes._2 + 'px !important',
	},

	'disabled': {
		color: colors.grayLow,
		backgroundColor: colors.lightGray,

		':hover': {
			backgroundColor: colors.lightGray
		},

		':active': {
			backgroundColor: colors.lightGray
		}
	},

	'wrapper_default': {
		borderStyle: 'solid',
		borderWidth: 1,
		borderColor: colors.yellowHigh,
		borderRadius: sizes._1,
		paddingTop: sizes._1 - 1,
		paddingBottom: sizes._1 - sizes._0 - 1,
		paddingLeft: sizes._1,
		paddingRight: sizes._1,

		':hover': {
			borderColor: colors.yellow
		}
	},

	'wrapper_disabled': {
		backgroundColor: colors.lightGray,
		borderColor: colors.grayLow,

		':hover': {
			borderColor: colors.grayLow
		},

		':active': {
			borderColor: colors.grayLow
		}
	},

	'wrapper_leftIcon': {
		paddingRight: sizes._1
	},

	'wrapper_rightIcon': {
		paddingLeft: sizes._1
	},

	'wrapper_hasError': {
		borderColor: colors.redHigh,
		boxShadow: `0 0 1px 0px ${colors.redHigh}`,

		':hover': {
			borderColor: colors.red
		},

		':active': {
			borderColor: colors.red
		}
	}
});