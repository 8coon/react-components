import {StyleSheet} from 'aphrodite/no-important';

import colors from '../vars/colors';

export default StyleSheet.create({
	'default': {
		backgroundColor: colors.transparentBlack
	}
});
