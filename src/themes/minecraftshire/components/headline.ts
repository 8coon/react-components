import {StyleSheet} from 'aphrodite/no-important';

import {headlineFont} from '../vars/fonts';
import colors from '../vars/colors';
import sizes from '../vars/sizes';

export default StyleSheet.create({
	'default': {
		...headlineFont,
		color: colors.black,
		paddingBottom: sizes._2
	}
});
