
export default {
	black: '#333',
	white: '#fff',

	gray: '#888',
	grayLow: '#6f6f6f',
	grayHigh: '#a2a2a2',

	lightGray: '#EEE',

	darkBlue: '#323E44',
	darkBlueLow: '#1c2327',
	darkBlueHigh: '#485961',

	blue: '#475E71',
	blueLow: '#334452',
	blueHigh: '#5b7890',

	darkGreen: '#426263',
	darkGreenLow: '#2e4444',
	darkGreenHigh: '#568082',

	lightGreen: '#A1BCAB',
	lightGreenLow: '#83a790',
	lightGreenHigh: '#bfd1c6',

	yellow: '#E3CD9F',
	yellowLow: '#d7b878',
	yellowHigh: '#efe2c6',

	orange: '#C77945',
	orangeLow: '#a76132',
	orangeHigh: '#d3956c',

	red: '#6F192D',
	redLow: '#45101c',
	redHigh: '#99223e',

	transparentBlack: 'rgba(51, 51, 51, .44)'
};
