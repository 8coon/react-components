import sizes, {defaultLineHeight} from './sizes';
import colors from './colors';

export const defaultFontSize = 15;

export const defaultFont = {
	fontFamily: ['San Francisco', 'Segoe UI', 'Roboto', 'Helvetica', 'Arial', 'sans-serif'],
	fontSize: `${defaultFontSize}px`,
	lineHeight: `${defaultLineHeight}px`,
	color: colors.black
};

export const headlineFont = {
	...defaultFont,
	fontSize: sizes._3,
	lineHeight: sizes._4 + 'px'
};
