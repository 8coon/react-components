import sizes from '../vars/sizes';

export const flicker = {
	func: prop => color => ({
		'0%': {
			[prop]: `0 0 ${sizes._3}px ${color}`
		},

		'50%': {
			[prop]: `0 0 ${sizes._2}px ${color}`
		},

		'100%': {
			[prop]: `0 0 ${sizes._3}px ${color}`
		},
	}),
	duration: '2s'
};

export default {
	flicker
};
