import {StyleSheet} from 'aphrodite/no-important';

import UIHorizontalLayout from './components/horizontal';
import UIVerticalLayout from './components/vertical';
import UIValidator from './components/validator';
import UIInput from './components/input';
import UIIcon from './components/icon';
import UIGroup from './components/group';
import UIButton from './components/button';
import UICurtain from './components/curtain';
import UIHeadline from './components/headline';
import UIText from './components/text';
import UIWindow from './components/window';
import UISpacer from './components/spacer';
import UIHr from './components/hr';

import animations from './vars/animations';
import colors from './vars/colors';
import fonts from './vars/fonts';
import sizes from './vars/sizes';

export default {
	UIHorizontalLayout,
	UIVerticalLayout,

	UIValidator,
	UIInput,
	UIIcon,
	UIGroup,
	UIButton,
	UICurtain,
	UIHeadline,
	UIText,
	UIWindow,
	UISpacer,
	UIHr,

	animations,
	colors,
	fonts,
	sizes
};
