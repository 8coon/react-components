import sizes, {defaultLineHeight} from './sizes';
import colors from './colors';

export const defaultFontSize = 15;

export const defaultFont = {
	fontFamily: ['San Francisco', 'Segoe UI', 'Roboto', 'Helvetica', 'Arial', 'sans-serif'],
	fontSize: `${defaultFontSize}px`,
	lineHeight: `${defaultLineHeight}px`,
	color: colors.richBlack
};

const step = 0.6;

export const h5Font = {
	...defaultFont,
	fontSize: defaultFontSize,
	lineHeight: sizes._6 + 'px',
	fontWeight: 700
};

export const h4Font = {
	...defaultFont,
	fontSize: defaultFontSize * 2 * step,
	lineHeight: sizes._8 + 'px',
	fontWeight: 700,
};

export const h3Font = {
	...defaultFont,
	fontSize: defaultFontSize * 3 * step,
	lineHeight: sizes._11 + 'px',
	fontWeight: 700,
};

export const h2Font = {
	...defaultFont,
	fontSize: defaultFontSize * 4 * step,
	lineHeight: sizes._14 + 'px',
	fontWeight: 700,
};

export const h1Font = {
	...defaultFont,
	fontSize: defaultFontSize * 5 * step,
	lineHeight: sizes._17 + 'px',
	fontWeight: 700,
};

