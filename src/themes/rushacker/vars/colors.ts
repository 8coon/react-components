
export default {
	richBlack: '#333333',
	darkSpringGreen: '#137044',
	eucalyptus: '#3CE093',
	springGreen: '#13F28A',
	darkSlateGray: '#2F5B47',
	white: '#FFFFFF',
	dimGray: '#F4F5F5',
	platinum: '#E7E9E8',
	lightGray: '#D0D3D2',
	gray: '#B9BDBB',
	darkGray: '#666666',
	desire: '#EF4050',
	yellow: '#FFD767',
	transparentBlack: 'rgba(0, 0, 0, .44)',
	transparentDimBlack: 'rgba(0, 0, 0, .77)',
	transparentGray: 'rgba(231, 233, 232, .44)',
	transparentDimGray: 'rgba(231, 233, 232, .22)'
};

export interface RGBA {
	r: number;
	g: number;
	b: number;
	a?: number;
}

export function hex2rgb(hex: string): RGBA {
	const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

	return result && {
		r: parseInt(result[1], 16),
		g: parseInt(result[2], 16),
		b: parseInt(result[3], 16)
	};
}

export function color2rgb(color: string): RGBA {
	if (color[0] === '#') {
		return hex2rgb(color);
	}

	const result = /^rgb[a]?\(?([\d]{1,3}),[\s]*([\d]{1,3}),[\s]*([\d]{1,3})[,]?[\s]*([.\d]*)\)$/i.exec(color);

	return result && {
		r: parseInt(result[1], 10),
		g: parseInt(result[2], 10),
		b: parseInt(result[3], 10),
		a: result[4] !== void 0 && parseFloat(result[4])
	}
}

export function rgba(color: RGBA, alpha: number = 0): string {
	return `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a !== void 0 ? color.a - alpha : alpha})`;
}

export function scrimGradient(params: string, color: string): string {
	const initial = color2rgb(color);

	return (
		'linear-gradient(' +
		params +
		rgba(initial) + ' 0%,' +
		rgba(initial, 0.738) + ' 19%,' +
		rgba(initial, 0.541) + ' 34%,' +
		rgba(initial, 0.382) + ' 47%,' +
		rgba(initial, 0.278) + ' 56.5%,' +
		rgba(initial, 0.194) + ' 65%,' +
		rgba(initial, 0.126) + ' 73%,' +
		rgba(initial, 0.075) + ' 80.2%,' +
		rgba(initial, 0.042) + ' 86.1%,' +
		rgba(initial, 0.021) + ' 91%,' +
		rgba(initial, 0.008) + ' 95.2%,' +
		rgba(initial, 0.002) + ' 98.2%,' +
		rgba(initial, 0) + ' 100%,' +
		')'
	);
}