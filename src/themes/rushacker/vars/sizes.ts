import sizes, {defaultLineHeight as d} from '../../../styles/sizes';

export default sizes;
export const defaultLineHeight = 20;
