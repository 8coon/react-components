import {StyleSheet} from 'aphrodite/no-important';

import colors from '../vars/colors';
import sizes from '../vars/sizes';

export default StyleSheet.create({
	'default': {
		backgroundColor: colors.transparentBlack
	}
});
