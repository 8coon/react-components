import {StyleSheet} from 'aphrodite/no-important';

import sizes from '../vars/sizes';
import {defaultFont} from '../vars/fonts';
import colors from '../vars/colors';

export default StyleSheet.create({
	'default': {
		...defaultFont
	},

	'errorBlock': {
		...defaultFont,
		color: colors.desire
	},

	'leftIcon': {
		paddingRight: sizes._1
	}
});
