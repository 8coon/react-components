import {StyleSheet} from 'aphrodite/no-important';

import sizes, {defaultLineHeight} from '../vars/sizes';
import {defaultFont} from '../vars/fonts';
import colors from '../vars/colors';

export const padding = sizes._2;

export default StyleSheet.create({
	'default': {
		...defaultFont,
		backgroundColor: colors.white,
		borderStyle: 'solid',
		borderWidth: 1,
		borderColor: colors.platinum,
		borderRadius: sizes._1
	},

	'hasTitle': {
		marginTop: sizes._1,
		paddingTop: 2 * sizes._1
	},

	'square': {
		borderRadius: 0
	},

	'title': {
		position: 'absolute',
		marginTop: -1 * defaultLineHeight,
		backgroundColor: colors.white,
		color: colors.gray,
		fontWeight: 700,
		paddingLeft: sizes._1,
		paddingRight: sizes._1
	},

	'pure': {
		borderStyle: 'none',
		borderWidth: 0
	},

	'borderNone': {
		paddingTop: 0,
		paddingBottom: 0,
		paddingLeft: 0,
		paddingRight: 0
	},

	'borderSingle': {
		paddingTop: padding,
		paddingBottom: padding,
		paddingLeft: padding,
		paddingRight: padding
	},

	'borderDouble': {
		paddingTop: 2 * padding,
		paddingBottom: 2 * padding,
		paddingLeft: 2 * padding,
		paddingRight: 2 * padding
	},

	'borderTriple': {
		paddingTop: 3 * padding,
		paddingBottom: 3 * padding,
		paddingLeft: 3 * padding,
		paddingRight: 3 * padding
	},

	'borderQuadruple': {
		paddingTop: 4 * padding,
		paddingBottom: 4 * padding,
		paddingLeft: 4 * padding,
		paddingRight: 4 * padding
	}
});