import {StyleSheet} from 'aphrodite/no-important';

import {defaultFont} from '../vars/fonts';
import colors from '../vars/colors';
import sizes from '../vars/sizes';

export default StyleSheet.create({
	'default': {
		...defaultFont,
		color: colors.richBlack,
		paddingBottom: sizes._2
	},

	'bare': {
		paddingBottom: 0
	}
});
