import {StyleSheet} from 'aphrodite/no-important';

import sizes from '../vars/sizes';
import {defaultFont} from '../vars/fonts';
import colors from '../vars/colors';

export default StyleSheet.create({
	'default': {
		...defaultFont,
		color: colors.white,
		backgroundColor: colors.gray,
		borderColor: colors.springGreen,
		borderRadius: sizes._1,
		paddingTop: sizes._1,
		paddingBottom: sizes._1,
		paddingLeft: sizes._2,
		paddingRight: sizes._2,
		height: sizes._8,
		transition: 'background-color .1s, color .1s',

		':hover': {
			backgroundColor: colors.lightGray,
			color: colors.white
		},

		':active': {
			backgroundColor: colors.lightGray,
			color: colors.white
		}
	},

	'disabled': {
		backgroundColor: colors.platinum,
		color: colors.lightGray,
		borderColor: colors.lightGray,

		':hover': {
			backgroundColor: colors.platinum,
			color: colors.lightGray
		},

		':active': {
			backgroundColor: colors.platinum,
			color: colors.lightGray
		}
	},

	'highlighted': {
		color: colors.white,
		backgroundColor: colors.eucalyptus,
		borderColor: colors.springGreen,
		fontWeight: 700,

		':hover': {
			backgroundColor: colors.springGreen,
			color: colors.white
		},

		':active': {
			backgroundColor: colors.eucalyptus,
			color: colors.white
		}
	},

	'link': {
		color: colors.eucalyptus,
		backgroundColor: 'transparent',
		paddingTop: 0,
		paddingRight: 0,
		paddingBottom: 0,
		paddingLeft: 0,

		':hover': {
			color: colors.springGreen,
			backgroundColor: 'transparent'
		},

		':active': {
			color: colors.springGreen,
			backgroundColor: 'transparent'
		}
	},

	'leftIcon': {
		paddingRight: sizes._2
	},

	'rightIcon': {
		paddingLeft: sizes._2
	},

	'pure': {
		color: colors.richBlack,
		backgroundColor: 'transparent',

		':hover': {
			backgroundColor: colors.platinum
		},

		':active': {
			backgroundColor: colors.lightGray
		}
	},

	'short': {
		paddingTop: sizes._1,
		paddingBottom: sizes._1,
		paddingLeft: sizes._1,
		paddingRight: sizes._1
	},

	'shortIcon': {
		paddingRight: 0,
		paddingLeft: 0
	}
});
