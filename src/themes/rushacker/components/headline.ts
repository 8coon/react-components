import {StyleSheet} from 'aphrodite/no-important';

import {h1Font, h2Font, h3Font, h4Font, h5Font} from '../vars/fonts';
import colors from '../vars/colors';
import sizes from '../vars/sizes';

const headlineStyle = {
	color: colors.richBlack,
	paddingBottom: sizes._3
};

export default StyleSheet.create({
	'h1': {
		...h1Font,
		...headlineStyle
	},

	'h2': {
		...h2Font,
		...headlineStyle
	},

	'h3': {
		...h3Font,
		...headlineStyle
	},

	'h4': {
		...h4Font,
		...headlineStyle
	},

	'h5': {
		...h5Font,
		...headlineStyle
	}
});
