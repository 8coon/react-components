import {StyleSheet} from 'aphrodite/no-important';
import sizes from '../vars/sizes';

export default StyleSheet.create({
	'child_padded': {
		paddingLeft: sizes._2,
		paddingRight: sizes._2
	}
});
