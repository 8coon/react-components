import {StyleSheet} from 'aphrodite/no-important';
import {padding} from './group';

export default StyleSheet.create({
	'none': {
		minWidth: 0,
		minHeight: 0
	},

	'single': {
		minWidth: padding,
		minHeight: padding
	},

	'double': {
		minWidth: 2 * padding,
		minHeight: 2 * padding
	},

	'triple': {
		minWidth: 3 * padding,
		minHeight: 3* padding
	},

	'quadruple': {
		minWidth: 4 * padding,
		minHeight: 4 * padding
	}
});
