import {StyleSheet} from 'aphrodite/no-important';
import sizes from '../vars/sizes';

export default StyleSheet.create({
	'child_padded': {
		paddingTop: sizes._2,
		paddingBottom: sizes._2
	}
});
