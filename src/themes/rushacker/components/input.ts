import {StyleSheet} from 'aphrodite/no-important';

import sizes from '../vars/sizes';
import {defaultFont} from '../vars/fonts';
import colors from '../vars/colors';

export default StyleSheet.create({
	'default': {
		...defaultFont,
		lineHeight: sizes._2 + 'px !important',

		'::placeholder': {
			color: colors.gray
		}
	},

	'disabled': {
		color: colors.white,
		backgroundColor: colors.platinum,

		':hover': {
			backgroundColor: colors.platinum
		},

		':active': {
			backgroundColor: colors.platinum
		}
	},

	'wrapper_default': {
		borderStyle: 'solid',
		borderWidth: 1,
		borderColor: colors.lightGray,
		borderRadius: sizes._1,
		paddingTop: sizes._1,
		paddingBottom: sizes._1,
		paddingLeft: sizes._1,
		paddingRight: sizes._1,
		height: sizes._8,
		transition: 'border-color .1s',

		':hover': {
			borderColor: colors.platinum
		}
	},

	'wrapper_disabled': {
		backgroundColor: colors.platinum,
		borderColor: colors.lightGray,

		':hover': {
			borderColor: colors.lightGray
		},

		':active': {
			borderColor: colors.lightGray
		}
	},

	'wrapper_leftIcon': {
		paddingRight: sizes._1
	},

	'wrapper_rightIcon': {
		paddingLeft: sizes._1
	},

	'wrapper_hasError': {
		borderColor: colors.desire,

		':hover': {
			borderColor: colors.desire
		},

		':active': {
			borderColor: colors.desire
		}
	}
});