import {StyleSheet} from 'aphrodite/no-important';
import colors from '../vars/colors';

export default StyleSheet.create({
	'default': {
		padding: 0,
		margin: 0,
		backgroundColor: colors.platinum,
		height: 1
	}
});
