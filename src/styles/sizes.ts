
const sizes =  {
	_0: 1,
	_1: 4,
	_2: 8,
	_3: 12,
	_4: 16,
	_5: 20,
	_6: 24,
	_7: 28,
	_8: 32,
	_9: 36,
	_10: 40,
	_11: 44,
	_12: 48,
	_13: 52,
	_14: 56,
	_15: 60,
	_16: 64,
	_17: 68
};

export const defaultLineHeight = sizes._5;
export default sizes;
