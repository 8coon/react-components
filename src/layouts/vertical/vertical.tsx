import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import ThemeStyleSheet from '../../themes/index';

export interface UIVerticalLayoutProps {
	bare?: boolean,
	grow?: boolean,
	raw?: boolean,
	justify?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around',
	align?: 'stretch' | 'center' | 'flex-start' | 'flex-end' | 'baseline'
}

const UIVerticalLayoutStylesheet = StyleSheet.create({
	'default': {
		display: 'flex',
		flexDirection: 'column',
		width: '100%'
	},

	'grow': {
		flex: 1
	},

	'child_default': {
		width: '100%'
	},

	'child_paddedFirst': {
		paddingTop: 0
	},

	'child_paddedLast': {
		paddingBottom: 0
	}
});

const UIVerticalLayout: React.StatelessComponent<UIVerticalLayoutProps> = props => {
	const isPadded = !props.bare;

	const childMapper = (child, i) => {
		const isFirst = i === 0;
		const isLast = i === React.Children.count(props.children) - 1;

		if (props.raw) {
			return child;
		}

		return <div className={css(
			UIVerticalLayoutStylesheet.child_default,
			ThemeStyleSheet.UIVerticalLayout.child_default,
			isPadded && UIVerticalLayoutStylesheet.child_padded,
			isPadded && ThemeStyleSheet.UIVerticalLayout.child_padded,
			isFirst && UIVerticalLayoutStylesheet.child_paddedFirst,
			isFirst && ThemeStyleSheet.UIVerticalLayout.child_paddedFirst,
			isLast && UIVerticalLayoutStylesheet.child_paddedLast,
			isLast && ThemeStyleSheet.UIVerticalLayout.child_paddedLast
		)}>
			{child}
		</div>;
	};

	return <div className={css(
		UIVerticalLayoutStylesheet.default,
		ThemeStyleSheet.UIVerticalLayout.default,
		props.grow && UIVerticalLayoutStylesheet.grow,
		props.grow && ThemeStyleSheet.UIVerticalLayout.grow
	)}
				style={{
					alignItems: props.align || 'flex-start',
					justifyContent: props.justify || 'flex-start'
				}}>
		{React.Children.map(props.children, childMapper)}
	</div>
};

export default UIVerticalLayout;
