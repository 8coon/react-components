import * as React from 'react';
import {StyleSheet, css} from 'aphrodite/no-important';

import ThemeStyleSheet from '../../themes/index';

export interface UIHorizontalLayoutProps {
	bare?: boolean,
	grow?: boolean,
	raw?: boolean,
	justify?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around',
	align?: 'stretch' | 'center' | 'flex-start' | 'flex-end' | 'baseline'
}

const UIHorizontalLayoutStylesheet = StyleSheet.create({
	'default': {
		display: 'flex',
		flexDirection: 'row',
		width: '100%'
	},

	'grow': {
		flex: 1
	},

	'child_default': {
		width: '100%'
	},

	'child_paddedFirst': {
		paddingLeft: 0
	},

	'child_paddedLast': {
		paddingRight: 0
	}
});


const UIHorizontalLayout: React.StatelessComponent<UIHorizontalLayoutProps> = props => {
	const isPadded = !props.bare;

	const childMapper = (child, i) => {
		const isFirst = i === 0;
		const isLast = i === React.Children.count(props.children) - 1;

		if (props.raw) {
			return child;
		}

		return <div className={css(
			UIHorizontalLayoutStylesheet.child_default,
			ThemeStyleSheet.UIHorizontalLayout.child_default,
			isPadded && UIHorizontalLayoutStylesheet.child_padded,
			isPadded && ThemeStyleSheet.UIHorizontalLayout.child_padded,
			isFirst && UIHorizontalLayoutStylesheet.child_paddedFirst,
			isFirst && ThemeStyleSheet.UIHorizontalLayout.child_paddedFirst,
			isLast && UIHorizontalLayoutStylesheet.child_paddedLast,
			isLast && ThemeStyleSheet.UIHorizontalLayout.child_paddedLast)}>
			{child}
		</div>
	};

	return <div className={css(
			UIHorizontalLayoutStylesheet.default,
			ThemeStyleSheet.UIHorizontalLayout.default,
			props.grow && UIHorizontalLayoutStylesheet.grow,
			props.grow && ThemeStyleSheet.UIHorizontalLayout.grow
		)} style={{
			alignItems: props.align || 'flex-start',
			justifyContent: props.justify || 'flex-start'
		}}>
		{React.Children.map(props.children, childMapper)}
	</div>
};

export default UIHorizontalLayout;
