export declare const defaultFontSize = 15;
export declare const defaultFont: {
    fontFamily: string[];
    fontSize: string;
    lineHeight: string;
    color: string;
};
export declare const headlineFont: {
    fontSize: number;
    lineHeight: string;
    fontFamily: string[];
    color: string;
};
