import fonts from './vars/fonts';
declare const _default: {
    UIHorizontalLayout: any;
    UIVerticalLayout: any;
    UIValidator: any;
    UIInput: any;
    UIIcon: any;
    UIGroup: any;
    UIButton: any;
    UICurtain: any;
    UIHeadline: any;
    UIText: any;
    UIWindow: any;
    UISpacer: any;
    UIHr: any;
    animations: {
        flicker: {
            func: (prop: any) => (color: any) => {
                '0%': {
                    [x: number]: string;
                };
                '50%': {
                    [x: number]: string;
                };
                '100%': {
                    [x: number]: string;
                };
            };
            duration: string;
        };
    };
    colors: {
        black: string;
        white: string;
        gray: string;
        grayLow: string;
        grayHigh: string;
        lightGray: string;
        darkBlue: string;
        darkBlueLow: string;
        darkBlueHigh: string;
        blue: string;
        blueLow: string;
        blueHigh: string;
        darkGreen: string;
        darkGreenLow: string;
        darkGreenHigh: string;
        lightGreen: string;
        lightGreenLow: string;
        lightGreenHigh: string;
        yellow: string;
        yellowLow: string;
        yellowHigh: string;
        orange: string;
        orangeLow: string;
        orangeHigh: string;
        red: string;
        redLow: string;
        redHigh: string;
        transparentBlack: string;
    };
    fonts: typeof fonts;
    sizes: {
        _0: number;
        _1: number;
        _2: number;
        _3: number;
        _4: number;
        _5: number;
        _6: number;
        _7: number;
        _8: number;
        _9: number;
        _10: number;
        _11: number;
        _12: number;
        _13: number;
        _14: number;
        _15: number;
        _16: number;
        _17: number;
    };
};
export default _default;
