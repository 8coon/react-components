/// <reference types="react" />
import * as React from 'react';
export interface UIVerticalLayoutProps {
    bare?: boolean;
    grow?: boolean;
    raw?: boolean;
    justify?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around';
    align?: 'stretch' | 'center' | 'flex-start' | 'flex-end' | 'baseline';
}
declare const UIVerticalLayout: React.StatelessComponent<UIVerticalLayoutProps>;
export default UIVerticalLayout;
