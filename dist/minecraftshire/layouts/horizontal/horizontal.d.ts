/// <reference types="react" />
import * as React from 'react';
export interface UIHorizontalLayoutProps {
    bare?: boolean;
    grow?: boolean;
    raw?: boolean;
    justify?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around';
    align?: 'stretch' | 'center' | 'flex-start' | 'flex-end' | 'baseline';
}
declare const UIHorizontalLayout: React.StatelessComponent<UIHorizontalLayoutProps>;
export default UIHorizontalLayout;
