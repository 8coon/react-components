/// <reference types="react" />
import * as React from 'react';
import { SVGNode } from '../icon/icon';
export interface UIInputProps {
    text?: string;
    placeholder?: string;
    password?: boolean;
    grow?: boolean;
    noAutoComplete?: boolean;
    disabled?: boolean;
    hasError?: boolean;
    leftIcon?: SVGNode;
    rightIcon?: SVGNode;
    icon?: SVGNode;
    autoFocus?: boolean;
    tabIndex?: number;
    onChange?: (event: React.SyntheticEvent<any>, value: string) => void;
}
declare const UIInput: React.StatelessComponent<UIInputProps>;
export default UIInput;
