/// <reference types="react" />
import * as React from 'react';
import { SVGNode } from '../icon/icon';
export interface UIValidatorProps {
    icon?: SVGNode;
    text?: string;
    grow?: boolean;
    bare?: boolean;
    validated?: boolean;
    justify?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around';
    align?: 'stretch' | 'center' | 'flex-start' | 'flex-end' | 'baseline';
    onValidate: (...values: string[]) => void;
}
declare const UIValidator: React.StatelessComponent<UIValidatorProps>;
export default UIValidator;
