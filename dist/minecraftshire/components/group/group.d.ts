/// <reference types="react" />
import * as React from 'react';
export interface UIGroupProps {
    grow?: boolean;
    text?: string;
    pure?: boolean;
    bare?: boolean;
    square?: boolean;
    verticalGrow?: boolean;
    justify?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around';
    align?: 'stretch' | 'center' | 'flex-start' | 'flex-end' | 'baseline';
    border?: 'none' | 'single' | 'double' | 'triple' | 'quadruple';
}
declare const UIGroup: React.StatelessComponent<UIGroupProps>;
export default UIGroup;
