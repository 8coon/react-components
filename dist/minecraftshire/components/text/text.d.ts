/// <reference types="react" />
import * as React from 'react';
export interface UITextProps {
    bare?: boolean;
}
declare const UIText: React.StatelessComponent<UITextProps>;
export default UIText;
