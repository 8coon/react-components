/// <reference types="react" />
import * as React from 'react';
export declare type onSubmitHandler = (event) => void;
export interface UIFormProps {
    onSubmit?: onSubmitHandler;
    grow?: boolean;
    pure?: boolean;
}
declare const UIForm: React.StatelessComponent<UIFormProps>;
export default UIForm;
