/// <reference types="react" />
import * as React from 'react';
export interface UIHeadlineProps {
    size?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5';
}
declare const UIHeadline: React.StatelessComponent<UIHeadlineProps>;
export default UIHeadline;
