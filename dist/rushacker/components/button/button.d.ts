/// <reference types="react" />
import * as React from 'react';
import { SVGNode } from '../icon/icon';
export interface UIButtonProps {
    text?: string;
    icon?: SVGNode;
    leftIcon?: SVGNode;
    rightIcon?: SVGNode;
    disabled?: boolean;
    highlighted?: boolean;
    grow?: boolean;
    link?: boolean;
    href?: string;
    target?: string;
    pure?: boolean;
    short?: boolean;
    leftIconHeight?: number;
    leftIconWidth?: number;
    leftIconFill?: string;
    rightIconHeight?: number;
    rightIconWidth?: number;
    rightIconFill?: string;
    iconHeight?: number;
    iconWidth?: number;
    iconFill?: string;
    onClick?: (event: React.SyntheticEvent<any>) => void;
}
declare const UIButton: React.StatelessComponent<UIButtonProps>;
export default UIButton;
