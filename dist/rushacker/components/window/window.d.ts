/// <reference types="react" />
import * as React from 'react';
export interface UIWindowProps {
    grow?: boolean;
}
declare const UIWindow: React.StatelessComponent<UIWindowProps>;
export default UIWindow;
