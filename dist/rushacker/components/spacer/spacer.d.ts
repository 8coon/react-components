/// <reference types="react" />
import * as React from 'react';
export interface UISpacerProps {
    size?: 'none' | 'single' | 'double' | 'triple' | 'quadruple';
}
declare const UISpacer: React.StatelessComponent<UISpacerProps>;
export default UISpacer;
