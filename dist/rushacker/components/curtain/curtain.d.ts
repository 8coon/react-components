/// <reference types="react" />
import * as React from 'react';
export interface UICurtainProps {
    zIndex?: number;
}
declare const UICurtain: React.StatelessComponent<UICurtainProps>;
export default UICurtain;
