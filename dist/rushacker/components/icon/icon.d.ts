/// <reference types="react" />
import * as React from 'react';
export interface SVGNode {
    name: string;
    attrs?: object;
    childs?: SVGNode[];
}
export interface UIIconProps {
    icon: SVGNode;
    width?: number;
    height?: number;
    fill?: string;
}
declare const UIIcon: React.StatelessComponent<UIIconProps>;
export default UIIcon;
