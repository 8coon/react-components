define(["aphrodite/no-important","react"], function(__WEBPACK_EXTERNAL_MODULE_0__, __WEBPACK_EXTERNAL_MODULE_2__) { return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var horizontal_1 = __webpack_require__(12);
var vertical_1 = __webpack_require__(14);
var validator_1 = __webpack_require__(15);
var input_1 = __webpack_require__(16);
var icon_1 = __webpack_require__(17);
var group_1 = __webpack_require__(8);
var button_1 = __webpack_require__(18);
var curtain_1 = __webpack_require__(19);
var headline_1 = __webpack_require__(20);
var text_1 = __webpack_require__(21);
var window_1 = __webpack_require__(22);
var spacer_1 = __webpack_require__(23);
var hr_1 = __webpack_require__(24);
var animations_1 = __webpack_require__(25);
var colors_1 = __webpack_require__(4), ColorUtils = colors_1;
var fonts = __webpack_require__(5);
var sizes_1 = __webpack_require__(3);
exports.default = {
    UIHorizontalLayout: horizontal_1.default,
    UIVerticalLayout: vertical_1.default,
    UIValidator: validator_1.default,
    UIInput: input_1.default,
    UIIcon: icon_1.default,
    UIGroup: group_1.default,
    UIButton: button_1.default,
    UICurtain: curtain_1.default,
    UIHeadline: headline_1.default,
    UIText: text_1.default,
    UIWindow: window_1.default,
    UISpacer: spacer_1.default,
    UIHr: hr_1.default,
    animations: animations_1.default,
    colors: colors_1.default,
    fonts: fonts,
    sizes: sizes_1.default,
    ColorUtils: ColorUtils
};


/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_2__;

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var sizes_1 = __webpack_require__(13);
exports.default = sizes_1.default;
exports.defaultLineHeight = 20;


/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    richBlack: '#333333',
    darkSpringGreen: '#137044',
    eucalyptus: '#3CE093',
    springGreen: '#13F28A',
    darkSlateGray: '#2F5B47',
    white: '#FFFFFF',
    dimGray: '#F4F5F5',
    platinum: '#E7E9E8',
    lightGray: '#D0D3D2',
    gray: '#B9BDBB',
    darkGray: '#666666',
    desire: '#EF4050',
    yellow: '#FFD767',
    transparentBlack: 'rgba(0, 0, 0, .44)',
    transparentDimBlack: 'rgba(0, 0, 0, .77)',
    transparentGray: 'rgba(231, 233, 232, .44)',
    transparentDimGray: 'rgba(231, 233, 232, .22)'
};
function hex2rgb(hex) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result && {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    };
}
exports.hex2rgb = hex2rgb;
function color2rgb(color) {
    if (color[0] === '#') {
        return hex2rgb(color);
    }
    var result = /^rgb[a]?\(?([\d]{1,3}),[\s]*([\d]{1,3}),[\s]*([\d]{1,3})[,]?[\s]*([.\d]*)\)$/i.exec(color);
    return result && {
        r: parseInt(result[1], 10),
        g: parseInt(result[2], 10),
        b: parseInt(result[3], 10),
        a: result[4] !== void 0 && parseFloat(result[4])
    };
}
exports.color2rgb = color2rgb;
function rgba(color, alpha) {
    if (alpha === void 0) { alpha = 0; }
    return "rgba(" + color.r + ", " + color.g + ", " + color.b + ", " + (color.a !== void 0 ? color.a - alpha : alpha) + ")";
}
exports.rgba = rgba;
function scrimGradient(params, color) {
    var initial = color2rgb(color);
    return ('linear-gradient(' +
        params +
        rgba(initial) + ' 0%,' +
        rgba(initial, 0.738) + ' 19%,' +
        rgba(initial, 0.541) + ' 34%,' +
        rgba(initial, 0.382) + ' 47%,' +
        rgba(initial, 0.278) + ' 56.5%,' +
        rgba(initial, 0.194) + ' 65%,' +
        rgba(initial, 0.126) + ' 73%,' +
        rgba(initial, 0.075) + ' 80.2%,' +
        rgba(initial, 0.042) + ' 86.1%,' +
        rgba(initial, 0.021) + ' 91%,' +
        rgba(initial, 0.008) + ' 95.2%,' +
        rgba(initial, 0.002) + ' 98.2%,' +
        rgba(initial, 0) + ' 100%,' +
        ')');
}
exports.scrimGradient = scrimGradient;


/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var sizes_1 = __webpack_require__(3);
var colors_1 = __webpack_require__(4);
exports.defaultFontSize = 15;
exports.defaultFont = {
    fontFamily: ['San Francisco', 'Segoe UI', 'Roboto', 'Helvetica', 'Arial', 'sans-serif'],
    fontSize: exports.defaultFontSize + "px",
    lineHeight: sizes_1.defaultLineHeight + "px",
    color: colors_1.default.richBlack
};
var step = 0.6;
exports.h5Font = __assign({}, exports.defaultFont, { fontSize: exports.defaultFontSize, lineHeight: sizes_1.default._6 + 'px', fontWeight: 700 });
exports.h4Font = __assign({}, exports.defaultFont, { fontSize: exports.defaultFontSize * 2 * step, lineHeight: sizes_1.default._8 + 'px', fontWeight: 700 });
exports.h3Font = __assign({}, exports.defaultFont, { fontSize: exports.defaultFontSize * 3 * step, lineHeight: sizes_1.default._11 + 'px', fontWeight: 700 });
exports.h2Font = __assign({}, exports.defaultFont, { fontSize: exports.defaultFontSize * 4 * step, lineHeight: sizes_1.default._14 + 'px', fontWeight: 700 });
exports.h1Font = __assign({}, exports.defaultFont, { fontSize: exports.defaultFontSize * 5 * step, lineHeight: sizes_1.default._17 + 'px', fontWeight: 700 });


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var createTag = function (node) { return React.createElement.apply(React, [node.name,
    Object
        .keys(node.attrs)
        .reduce(function (props, prevKey) {
        var nextKey = prevKey;
        if (prevKey === 'viewportFill') {
            nextKey = prevKey.toLowerCase();
        }
        props[nextKey] = node.attrs[prevKey];
        return props;
    }, {})].concat((node.childs ? node.childs.map(function (child) { return createTag(child); }) : []))); };
var UIIconStylesheet = no_important_1.StyleSheet.create({
    'default': {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
var UIIcon = function (props) {
    var icon = props.icon;
    props.width && (icon.attrs['width'] = props.width);
    props.height && (icon.attrs['height'] = props.height);
    props.fill && (icon.attrs['fill'] = props.fill);
    return (React.createElement("span", { className: no_important_1.css(UIIconStylesheet.default, index_1.default.UIIcon.default) }, createTag(props.icon)));
};
exports.default = UIIcon;


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var UIVerticalLayoutStylesheet = no_important_1.StyleSheet.create({
    'default': {
        display: 'flex',
        flexDirection: 'column',
        width: '100%'
    },
    'grow': {
        flex: 1
    },
    'child_default': {
        width: '100%'
    },
    'child_paddedFirst': {
        paddingTop: 0
    },
    'child_paddedLast': {
        paddingBottom: 0
    }
});
var UIVerticalLayout = function (props) {
    var isPadded = !props.bare;
    var childMapper = function (child, i) {
        var isFirst = i === 0;
        var isLast = i === React.Children.count(props.children) - 1;
        if (props.raw) {
            return child;
        }
        return React.createElement("div", { className: no_important_1.css(UIVerticalLayoutStylesheet.child_default, index_1.default.UIVerticalLayout.child_default, isPadded && UIVerticalLayoutStylesheet.child_padded, isPadded && index_1.default.UIVerticalLayout.child_padded, isFirst && UIVerticalLayoutStylesheet.child_paddedFirst, isFirst && index_1.default.UIVerticalLayout.child_paddedFirst, isLast && UIVerticalLayoutStylesheet.child_paddedLast, isLast && index_1.default.UIVerticalLayout.child_paddedLast) }, child);
    };
    return React.createElement("div", { className: no_important_1.css(UIVerticalLayoutStylesheet.default, index_1.default.UIVerticalLayout.default, props.grow && UIVerticalLayoutStylesheet.grow, props.grow && index_1.default.UIVerticalLayout.grow), style: {
            alignItems: props.align || 'flex-start',
            justifyContent: props.justify || 'flex-start'
        } }, React.Children.map(props.children, childMapper));
};
exports.default = UIVerticalLayout;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var sizes_1 = __webpack_require__(3);
var fonts_1 = __webpack_require__(5);
var colors_1 = __webpack_require__(4);
exports.padding = sizes_1.default._2;
exports.default = no_important_1.StyleSheet.create({
    'default': __assign({}, fonts_1.defaultFont, { backgroundColor: colors_1.default.white, borderStyle: 'solid', borderWidth: 1, borderColor: colors_1.default.platinum, borderRadius: sizes_1.default._1 }),
    'hasTitle': {
        marginTop: sizes_1.default._1,
        paddingTop: 2 * sizes_1.default._1
    },
    'square': {
        borderRadius: 0
    },
    'title': {
        position: 'absolute',
        marginTop: -1 * sizes_1.defaultLineHeight,
        backgroundColor: colors_1.default.white,
        color: colors_1.default.gray,
        fontWeight: 700,
        paddingLeft: sizes_1.default._1,
        paddingRight: sizes_1.default._1
    },
    'pure': {
        borderStyle: 'none',
        borderWidth: 0
    },
    'borderNone': {
        paddingTop: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        paddingRight: 0
    },
    'borderSingle': {
        paddingTop: exports.padding,
        paddingBottom: exports.padding,
        paddingLeft: exports.padding,
        paddingRight: exports.padding
    },
    'borderDouble': {
        paddingTop: 2 * exports.padding,
        paddingBottom: 2 * exports.padding,
        paddingLeft: 2 * exports.padding,
        paddingRight: 2 * exports.padding
    },
    'borderTriple': {
        paddingTop: 3 * exports.padding,
        paddingBottom: 3 * exports.padding,
        paddingLeft: 3 * exports.padding,
        paddingRight: 3 * exports.padding
    },
    'borderQuadruple': {
        paddingTop: 4 * exports.padding,
        paddingBottom: 4 * exports.padding,
        paddingLeft: 4 * exports.padding,
        paddingRight: 4 * exports.padding
    }
});


/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var UIGroupStyle = no_important_1.StyleSheet.create({
    'default': {
        boxSizing: 'border-box',
        display: 'inline-flex'
    },
    'grow': {
        width: '100%'
    },
    'borderNone': {},
    'borderSingle': {},
    'borderDouble': {},
    'borderTriple': {},
    'borderQuadruple': {},
    'square': {},
    'title': {
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap'
    },
    'verticalGrow': {
        display: 'flex',
        flex: 1
    }
});
var UIGroup = function (props) {
    var justify = props.justify || 'center';
    var align = props.align || 'flex-start';
    var border = props.bare ? 'none' : props.border || 'single';
    var borderNone = border === 'none';
    var borderSingle = border === 'single';
    var borderDouble = border === 'double';
    var borderTriple = border === 'triple';
    var borderQuadruple = border === 'quadruple';
    return (React.createElement("span", { className: no_important_1.css(UIGroupStyle.default, index_1.default.UIGroup.default, props.grow && UIGroupStyle.grow, props.grow && index_1.default.UIGroup.grow, props.text && UIGroupStyle.hasTitle, props.text && index_1.default.UIGroup.hasTitle, props.pure && UIGroupStyle.pure, props.pure && index_1.default.UIGroup.pure, props.verticalGrow && UIGroupStyle.verticalGrow, props.verticalGrow && index_1.default.UIGroup.verticalGrow, borderNone && UIGroupStyle.borderNone, borderNone && index_1.default.UIGroup.borderNone, borderSingle && UIGroupStyle.borderSingle, borderSingle && index_1.default.UIGroup.borderSingle, borderDouble && UIGroupStyle.borderDouble, borderDouble && index_1.default.UIGroup.borderDouble, borderTriple && UIGroupStyle.borderTriple, borderTriple && index_1.default.UIGroup.borderTriple, borderQuadruple && UIGroupStyle.borderQuadruple, borderQuadruple && index_1.default.UIGroup.borderQuadruple, props.square && UIGroupStyle.square, props.square && index_1.default.UIGroup.square), style: {
            alignItems: align,
            justifyContent: justify
        } },
        props.text && (React.createElement("span", { className: no_important_1.css(UIGroupStyle.title, index_1.default.UIGroup.title) }, props.text)),
        props.children));
};
exports.default = UIGroup;


/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
// Компоненты
var button_1 = __webpack_require__(11);
exports.UIButton = button_1.default;
var group_1 = __webpack_require__(9);
exports.UIGroup = group_1.default;
var icon_1 = __webpack_require__(6);
exports.UIIcon = icon_1.default;
var input_1 = __webpack_require__(26);
exports.UIInput = input_1.default;
var validator_1 = __webpack_require__(27);
exports.UIValidator = validator_1.default;
var curtain_1 = __webpack_require__(28);
exports.UICurtain = curtain_1.default;
var headline_1 = __webpack_require__(29);
exports.UIHeadline = headline_1.default;
var text_1 = __webpack_require__(30);
exports.UIText = text_1.default;
var window_1 = __webpack_require__(31);
exports.UIWindow = window_1.default;
var form_1 = __webpack_require__(32);
exports.UIForm = form_1.default;
var spacer_1 = __webpack_require__(33);
exports.UISpacer = spacer_1.default;
var hr_1 = __webpack_require__(34);
exports.UIHr = hr_1.default;
// Контейнеры
var horizontal_1 = __webpack_require__(35);
exports.UIHorizontalLayout = horizontal_1.default;
var vertical_1 = __webpack_require__(7);
exports.UIVerticalLayout = vertical_1.default;
// Выбранная тема
var themes_1 = __webpack_require__(36);
exports.ThemeStyleSheet = themes_1.default;


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var icon_1 = __webpack_require__(6);
var index_1 = __webpack_require__(1);
var UIButtonStyle = no_important_1.StyleSheet.create({
    'default': {
        cursor: 'pointer',
        userSelect: 'none',
        display: 'inline-flex',
        boxSizing: 'border-box',
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        textDecoration: 'none'
    },
    'disabled': {
        cursor: 'default'
    },
    'grow': {
        width: '100%'
    },
    'link': {
        display: 'inline',
        userSelect: 'auto',
        ':hover': {
            textDecoration: 'underline'
        }
    }
});
var UIButton = function (_p) {
    var props = __assign({}, _p);
    props.leftIcon = props.leftIcon || props.icon;
    props.leftIconHeight = props.leftIconHeight || props.iconHeight;
    props.leftIconWidth = props.leftIconWidth || props.iconWidth;
    props.leftIconFill = props.leftIconFill || props.iconFill;
    return (React.createElement("a", { className: no_important_1.css(UIButtonStyle.default, index_1.default.UIButton.default, props.disabled && UIButtonStyle.disabled, props.disabled && index_1.default.UIButton.disabled, props.highlighted && !props.disabled && UIButtonStyle.highlighted, props.highlighted && !props.disabled && index_1.default.UIButton.highlighted, props.grow && UIButtonStyle.grow, props.grow && index_1.default.UIButton.grow, props.link && UIButtonStyle.link, props.link && index_1.default.UIButton.link, props.pure && UIButtonStyle.pure, props.pure && index_1.default.UIButton.pure, props.short && UIButtonStyle.short, props.short && index_1.default.UIButton.short), onClick: props.onClick, href: props.href, target: props.target },
        props.leftIcon && (React.createElement("span", { className: no_important_1.css(UIButtonStyle.leftIcon, index_1.default.UIButton.leftIcon, props.short && UIButtonStyle.shortIcon, props.short && index_1.default.UIButton.shortIcon) },
            React.createElement(icon_1.default, { icon: props.leftIcon, height: props.leftIconHeight, width: props.leftIconWidth, fill: props.leftIconFill }))),
        !props.short && props.text,
        props.rightIcon && (React.createElement("span", { className: no_important_1.css(UIButtonStyle.rightIcon, index_1.default.UIButton.rightIcon, props.short && UIButtonStyle.shortIcon, props.short && index_1.default.UIButton.shortIcon) },
            React.createElement(icon_1.default, { icon: props.rightIcon, height: props.rightIconHeight, width: props.rightIconWidth, fill: props.rightIconFill })))));
};
exports.default = UIButton;


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var sizes_1 = __webpack_require__(3);
exports.default = no_important_1.StyleSheet.create({
    'child_padded': {
        paddingLeft: sizes_1.default._2,
        paddingRight: sizes_1.default._2
    }
});


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var sizes = {
    _0: 1,
    _1: 4,
    _2: 8,
    _3: 12,
    _4: 16,
    _5: 20,
    _6: 24,
    _7: 28,
    _8: 32,
    _9: 36,
    _10: 40,
    _11: 44,
    _12: 48,
    _13: 52,
    _14: 56,
    _15: 60,
    _16: 64,
    _17: 68
};
exports.defaultLineHeight = sizes._5;
exports.default = sizes;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var sizes_1 = __webpack_require__(3);
exports.default = no_important_1.StyleSheet.create({
    'child_padded': {
        paddingTop: sizes_1.default._2,
        paddingBottom: sizes_1.default._2
    }
});


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var sizes_1 = __webpack_require__(3);
var fonts_1 = __webpack_require__(5);
var colors_1 = __webpack_require__(4);
exports.default = no_important_1.StyleSheet.create({
    'default': __assign({}, fonts_1.defaultFont),
    'errorBlock': __assign({}, fonts_1.defaultFont, { color: colors_1.default.desire }),
    'leftIcon': {
        paddingRight: sizes_1.default._1
    }
});


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var sizes_1 = __webpack_require__(3);
var fonts_1 = __webpack_require__(5);
var colors_1 = __webpack_require__(4);
exports.default = no_important_1.StyleSheet.create({
    'default': __assign({}, fonts_1.defaultFont, { lineHeight: sizes_1.default._2 + 'px !important', '::placeholder': {
            color: colors_1.default.gray
        } }),
    'disabled': {
        color: colors_1.default.white,
        backgroundColor: colors_1.default.platinum,
        ':hover': {
            backgroundColor: colors_1.default.platinum
        },
        ':active': {
            backgroundColor: colors_1.default.platinum
        }
    },
    'wrapper_default': {
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: colors_1.default.lightGray,
        borderRadius: sizes_1.default._1,
        paddingTop: sizes_1.default._1,
        paddingBottom: sizes_1.default._1,
        paddingLeft: sizes_1.default._1,
        paddingRight: sizes_1.default._1,
        height: sizes_1.default._8,
        transition: 'border-color .1s',
        ':hover': {
            borderColor: colors_1.default.platinum
        }
    },
    'wrapper_disabled': {
        backgroundColor: colors_1.default.platinum,
        borderColor: colors_1.default.lightGray,
        ':hover': {
            borderColor: colors_1.default.lightGray
        },
        ':active': {
            borderColor: colors_1.default.lightGray
        }
    },
    'wrapper_leftIcon': {
        paddingRight: sizes_1.default._1
    },
    'wrapper_rightIcon': {
        paddingLeft: sizes_1.default._1
    },
    'wrapper_hasError': {
        borderColor: colors_1.default.desire,
        ':hover': {
            borderColor: colors_1.default.desire
        },
        ':active': {
            borderColor: colors_1.default.desire
        }
    }
});


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
exports.default = no_important_1.StyleSheet.create({});


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var sizes_1 = __webpack_require__(3);
var fonts_1 = __webpack_require__(5);
var colors_1 = __webpack_require__(4);
exports.default = no_important_1.StyleSheet.create({
    'default': __assign({}, fonts_1.defaultFont, { color: colors_1.default.white, backgroundColor: colors_1.default.gray, borderColor: colors_1.default.springGreen, borderRadius: sizes_1.default._1, paddingTop: sizes_1.default._1, paddingBottom: sizes_1.default._1, paddingLeft: sizes_1.default._2, paddingRight: sizes_1.default._2, height: sizes_1.default._8, transition: 'background-color .1s, color .1s', ':hover': {
            backgroundColor: colors_1.default.lightGray,
            color: colors_1.default.white
        }, ':active': {
            backgroundColor: colors_1.default.lightGray,
            color: colors_1.default.white
        } }),
    'disabled': {
        backgroundColor: colors_1.default.platinum,
        color: colors_1.default.lightGray,
        borderColor: colors_1.default.lightGray,
        ':hover': {
            backgroundColor: colors_1.default.platinum,
            color: colors_1.default.lightGray
        },
        ':active': {
            backgroundColor: colors_1.default.platinum,
            color: colors_1.default.lightGray
        }
    },
    'highlighted': {
        color: colors_1.default.white,
        backgroundColor: colors_1.default.eucalyptus,
        borderColor: colors_1.default.springGreen,
        fontWeight: 700,
        ':hover': {
            backgroundColor: colors_1.default.springGreen,
            color: colors_1.default.white
        },
        ':active': {
            backgroundColor: colors_1.default.eucalyptus,
            color: colors_1.default.white
        }
    },
    'link': {
        color: colors_1.default.eucalyptus,
        backgroundColor: 'transparent',
        paddingTop: 0,
        paddingRight: 0,
        paddingBottom: 0,
        paddingLeft: 0,
        ':hover': {
            color: colors_1.default.springGreen,
            backgroundColor: 'transparent'
        },
        ':active': {
            color: colors_1.default.springGreen,
            backgroundColor: 'transparent'
        }
    },
    'leftIcon': {
        paddingRight: sizes_1.default._2
    },
    'rightIcon': {
        paddingLeft: sizes_1.default._2
    },
    'pure': {
        color: colors_1.default.richBlack,
        backgroundColor: 'transparent',
        ':hover': {
            backgroundColor: colors_1.default.platinum
        },
        ':active': {
            backgroundColor: colors_1.default.lightGray
        }
    },
    'short': {
        paddingTop: sizes_1.default._1,
        paddingBottom: sizes_1.default._1,
        paddingLeft: sizes_1.default._1,
        paddingRight: sizes_1.default._1
    },
    'shortIcon': {
        paddingRight: 0,
        paddingLeft: 0
    }
});


/***/ }),
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var colors_1 = __webpack_require__(4);
exports.default = no_important_1.StyleSheet.create({
    'default': {
        backgroundColor: colors_1.default.transparentBlack
    }
});


/***/ }),
/* 20 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var fonts_1 = __webpack_require__(5);
var colors_1 = __webpack_require__(4);
var sizes_1 = __webpack_require__(3);
var headlineStyle = {
    color: colors_1.default.richBlack,
    paddingBottom: sizes_1.default._3
};
exports.default = no_important_1.StyleSheet.create({
    'h1': __assign({}, fonts_1.h1Font, headlineStyle),
    'h2': __assign({}, fonts_1.h2Font, headlineStyle),
    'h3': __assign({}, fonts_1.h3Font, headlineStyle),
    'h4': __assign({}, fonts_1.h4Font, headlineStyle),
    'h5': __assign({}, fonts_1.h5Font, headlineStyle)
});


/***/ }),
/* 21 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var fonts_1 = __webpack_require__(5);
var colors_1 = __webpack_require__(4);
var sizes_1 = __webpack_require__(3);
exports.default = no_important_1.StyleSheet.create({
    'default': __assign({}, fonts_1.defaultFont, { color: colors_1.default.richBlack, paddingBottom: sizes_1.default._2 }),
    'bare': {
        paddingBottom: 0
    }
});


/***/ }),
/* 22 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var colors_1 = __webpack_require__(4);
var sizes_1 = __webpack_require__(3);
exports.default = no_important_1.StyleSheet.create({
    'default': {
        borderRadius: sizes_1.default._1,
        boxShadow: "0 0 20px -2px " + colors_1.default.transparentBlack,
        backgroundColor: colors_1.default.white,
        padding: sizes_1.default._3,
        width: 420
    },
    'grow': {
        borderRadius: 0,
        boxShadow: 'none'
    }
});


/***/ }),
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var group_1 = __webpack_require__(8);
exports.default = no_important_1.StyleSheet.create({
    'none': {
        minWidth: 0,
        minHeight: 0
    },
    'single': {
        minWidth: group_1.padding,
        minHeight: group_1.padding
    },
    'double': {
        minWidth: 2 * group_1.padding,
        minHeight: 2 * group_1.padding
    },
    'triple': {
        minWidth: 3 * group_1.padding,
        minHeight: 3 * group_1.padding
    },
    'quadruple': {
        minWidth: 4 * group_1.padding,
        minHeight: 4 * group_1.padding
    }
});


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = __webpack_require__(0);
var colors_1 = __webpack_require__(4);
exports.default = no_important_1.StyleSheet.create({
    'default': {
        padding: 0,
        margin: 0,
        backgroundColor: colors_1.default.platinum,
        height: 1
    }
});


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {};


/***/ }),
/* 26 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var icon_1 = __webpack_require__(6);
var index_1 = __webpack_require__(1);
var UIInputStyle = no_important_1.StyleSheet.create({
    'default': {
        outline: 0,
        borderStyle: 'none',
        borderWidth: 0,
        boxSizing: 'border-box',
        width: '100%'
    },
    'disabled': {
        cursor: 'default'
    },
    'wrapper_default': {
        borderStyle: 'solid',
        borderWidth: 1,
        boxSizing: 'border-box',
        display: 'inline-flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    'wrapper_disabled': {
        cursor: 'default'
    },
    'wrapper_grow': {
        width: '100%'
    }
});
var UIInput = function (_p) {
    var props = __assign({}, _p);
    props.leftIcon = props.leftIcon || props.icon;
    var inputAttrs = {};
    if (props.noAutoComplete) {
        inputAttrs['autocomplete'] = 'nope';
    }
    if (props.disabled) {
        inputAttrs['disabled'] = true;
    }
    var isGrow = props.grow;
    var isDisabled = props.disabled;
    var hasError = props.hasError;
    return (React.createElement("span", { className: no_important_1.css(UIInputStyle.wrapper_default, index_1.default.UIInput.wrapper_default, isGrow && UIInputStyle.wrapper_grow, isGrow && index_1.default.UIInput.wrapper_grow, isDisabled && UIInputStyle.wrapper_disabled, isDisabled && index_1.default.UIInput.wrapper_disabled, hasError && UIInputStyle.wrapper_hasError, hasError && index_1.default.UIInput.wrapper_hasError) },
        props.leftIcon && (React.createElement("span", { className: no_important_1.css(UIInputStyle.wrapper_leftIcon, index_1.default.UIInput.wrapper_leftIcon) },
            React.createElement(icon_1.default, { icon: props.leftIcon }))),
        React.createElement("input", __assign({ value: props.text || '' }, inputAttrs, { onChange: function (event) { return props.onChange && props.onChange(event, event.target.value); }, placeholder: props.placeholder, tabIndex: props.tabIndex !== void 0 ? props.tabIndex : -1, autoFocus: props.autoFocus }, (props.password ? { type: 'password' } : {}), { className: no_important_1.css(UIInputStyle.default, index_1.default.UIInput.default, props.disabled && UIInputStyle.disabled, props.disabled && index_1.default.UIInput.disabled) })),
        props.rightIcon && (React.createElement("span", { className: no_important_1.css(UIInputStyle.wrapper_rightIcon, index_1.default.UIInput.wrapper_rightIcon) },
            React.createElement(icon_1.default, { icon: props.rightIcon })))));
};
exports.default = UIInput;


/***/ }),
/* 27 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var icon_1 = __webpack_require__(6);
var vertical_1 = __webpack_require__(7);
var index_1 = __webpack_require__(1);
var UIValidatorStyle = no_important_1.StyleSheet.create({
    'default': {
        display: 'inline-block'
    },
    'grow': {
        width: '100%'
    },
    'errorBlock': {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    'errorBlockEmpty': {
        height: 0
    },
    'leftIcon': {}
});
var UIValidator = function (props) {
    var hasError = props.icon || props.text;
    var values = [];
    React.Children
        .forEach(props.children, function (child) {
        if (!React.isValidElement(child)) {
            return;
        }
        values.push(child.props['text']);
    });
    var onChildChange = function () {
        props.onValidate.apply(props, values);
    };
    if (props.validated === false) {
        onChildChange();
    }
    var isGrow = props.grow;
    var noError = !hasError;
    return (React.createElement("span", { className: no_important_1.css(UIValidatorStyle.default, index_1.default.UIValidator.default, isGrow && UIValidatorStyle.grow, isGrow && index_1.default.UIValidator.grow) },
        React.createElement(vertical_1.default, { bare: props.bare, justify: props.justify, align: props.align },
            React.Children.map(props.children, function (child, i) {
                if (!React.isValidElement(child)) {
                    return { child: child };
                }
                var onChange = child.props['onChange'];
                return React.cloneElement(child, {
                    key: i,
                    grow: child.props['grow'] === void 0 ? props.grow : child.props['grow'],
                    hasError: child.props['hasError'] === void 0 ? hasError : child.props['hasError'],
                    onChange: function (event) {
                        var args = [];
                        for (var _i = 1; _i < arguments.length; _i++) {
                            args[_i - 1] = arguments[_i];
                        }
                        onChange && onChange.apply(void 0, [event].concat(args));
                        values[i] = event.target['value'];
                        onChildChange();
                    }
                });
            }),
            React.createElement("span", { className: no_important_1.css(UIValidatorStyle.errorBlock, index_1.default.UIValidator.errorBlock, noError && UIValidatorStyle.errorBlockEmpty, noError && index_1.default.UIValidator.errorBlockEmpty) },
                props.icon && React.createElement("span", { className: no_important_1.css(UIValidatorStyle.leftIcon, index_1.default.UIValidator.leftIcon) },
                    React.createElement(icon_1.default, { icon: props.icon })),
                props.text && React.createElement("span", null, props.text)))));
};
exports.default = UIValidator;


/***/ }),
/* 28 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var UICurtainStyle = no_important_1.StyleSheet.create({
    'default': {
        boxSizing: 'border-box',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'fixed',
        overflow: 'auto',
        height: '100%',
        width: '100%',
        left: 0,
        top: 0
    }
});
var UICurtain = function (props) {
    var zIndex = props.zIndex || 0;
    return (React.createElement("div", { className: no_important_1.css(UICurtainStyle.default, index_1.default.UICurtain.default), style: { zIndex: zIndex } }, props.children));
};
exports.default = UICurtain;


/***/ }),
/* 29 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var UIHeadlineStyle = no_important_1.StyleSheet.create({});
var UIHeadline = function (props) {
    var size = props.size || 'h1';
    return (React.createElement("span", { className: no_important_1.css(index_1.default.UIHeadline[size]) }, props.children));
};
exports.default = UIHeadline;


/***/ }),
/* 30 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var UITextStyle = no_important_1.StyleSheet.create({
    'default': {
        display: 'block'
    },
    'bare': {}
});
var UIText = function (props) {
    return (React.createElement("span", { className: no_important_1.css(UITextStyle.default, index_1.default.UIText.default, props.bare && UITextStyle.bare, props.bare && index_1.default.UIText.bare) }, props.children));
};
exports.default = UIText;


/***/ }),
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var UIWindowStyle = no_important_1.StyleSheet.create({
    'default': {
        display: 'block'
    },
    'grow': {
        flex: 1
    }
});
var UIWindow = function (props) {
    return (React.createElement("div", { className: no_important_1.css(UIWindowStyle.default, index_1.default.UIWindow.default, props.grow && UIWindowStyle.grow, props.grow && index_1.default.UIWindow.grow) }, props.children));
};
exports.default = UIWindow;


/***/ }),
/* 32 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var group_1 = __webpack_require__(9);
var vertical_1 = __webpack_require__(7);
var index_1 = __webpack_require__(1);
var UIFormStyle = no_important_1.StyleSheet.create({
    'default': {}
});
var UIForm = function (props) {
    var onSubmit = function (event) {
        event && event.preventDefault();
        props.onSubmit && props.onSubmit(event);
    };
    return (React.createElement(group_1.default, { grow: props.grow, pure: props.pure },
        React.createElement(vertical_1.default, { bare: true },
            React.createElement("form", { onSubmit: onSubmit, className: no_important_1.css(UIFormStyle.default, index_1.default.UIText.default) },
                props.children,
                React.createElement("button", { type: "submit", style: { display: 'none' } }, "Send")))));
};
exports.default = UIForm;


/***/ }),
/* 33 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var UISpacer = function (props) {
    var size = props.size || 'single';
    var none = size === 'none';
    var single = size === 'single';
    var double = size === 'double';
    var triple = size === 'triple';
    var quadruple = size === 'quadruple';
    return (React.createElement("div", { className: no_important_1.css(none && index_1.default.UISpacer.none, single && index_1.default.UISpacer.single, double && index_1.default.UISpacer.double, triple && index_1.default.UISpacer.triple, quadruple && index_1.default.UISpacer.quadruple) }));
};
exports.default = UISpacer;


/***/ }),
/* 34 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var UIHr = function (props) {
    return (React.createElement("div", { className: no_important_1.css(index_1.default.UIHr.default) }));
};
exports.default = UIHr;


/***/ }),
/* 35 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var React = __webpack_require__(2);
var no_important_1 = __webpack_require__(0);
var index_1 = __webpack_require__(1);
var UIHorizontalLayoutStylesheet = no_important_1.StyleSheet.create({
    'default': {
        display: 'flex',
        flexDirection: 'row',
        width: '100%'
    },
    'grow': {
        flex: 1
    },
    'child_default': {
        width: '100%'
    },
    'child_paddedFirst': {
        paddingLeft: 0
    },
    'child_paddedLast': {
        paddingRight: 0
    }
});
var UIHorizontalLayout = function (props) {
    var isPadded = !props.bare;
    var childMapper = function (child, i) {
        var isFirst = i === 0;
        var isLast = i === React.Children.count(props.children) - 1;
        if (props.raw) {
            return child;
        }
        return React.createElement("div", { className: no_important_1.css(UIHorizontalLayoutStylesheet.child_default, index_1.default.UIHorizontalLayout.child_default, isPadded && UIHorizontalLayoutStylesheet.child_padded, isPadded && index_1.default.UIHorizontalLayout.child_padded, isFirst && UIHorizontalLayoutStylesheet.child_paddedFirst, isFirst && index_1.default.UIHorizontalLayout.child_paddedFirst, isLast && UIHorizontalLayoutStylesheet.child_paddedLast, isLast && index_1.default.UIHorizontalLayout.child_paddedLast) }, child);
    };
    return React.createElement("div", { className: no_important_1.css(UIHorizontalLayoutStylesheet.default, index_1.default.UIHorizontalLayout.default, props.grow && UIHorizontalLayoutStylesheet.grow, props.grow && index_1.default.UIHorizontalLayout.grow), style: {
            alignItems: props.align || 'flex-start',
            justifyContent: props.justify || 'flex-start'
        } }, React.Children.map(props.children, childMapper));
};
exports.default = UIHorizontalLayout;


/***/ }),
/* 36 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var index_1 = __webpack_require__(1);
exports.default = index_1.default;


/***/ })
/******/ ])});;
//# sourceMappingURL=index.min.js.map