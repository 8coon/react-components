export declare const flicker: {
    func: (prop: any) => (color: any) => {
        '0%': {
            [x: number]: string;
        };
        '50%': {
            [x: number]: string;
        };
        '100%': {
            [x: number]: string;
        };
    };
    duration: string;
};
declare const _default: {
    flicker: {
        func: (prop: any) => (color: any) => {
            '0%': {
                [x: number]: string;
            };
            '50%': {
                [x: number]: string;
            };
            '100%': {
                [x: number]: string;
            };
        };
        duration: string;
    };
};
export default _default;
