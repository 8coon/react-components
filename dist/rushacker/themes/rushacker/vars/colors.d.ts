declare const _default: {
    richBlack: string;
    darkSpringGreen: string;
    eucalyptus: string;
    springGreen: string;
    darkSlateGray: string;
    white: string;
    dimGray: string;
    platinum: string;
    lightGray: string;
    gray: string;
    darkGray: string;
    desire: string;
    yellow: string;
    transparentBlack: string;
    transparentDimBlack: string;
    transparentGray: string;
    transparentDimGray: string;
};
export default _default;
export interface RGBA {
    r: number;
    g: number;
    b: number;
    a?: number;
}
export declare function hex2rgb(hex: string): RGBA;
export declare function color2rgb(color: string): RGBA;
export declare function rgba(color: RGBA, alpha?: number): string;
export declare function scrimGradient(params: string, color: string): string;
