export declare const defaultFontSize = 15;
export declare const defaultFont: {
    fontFamily: string[];
    fontSize: string;
    lineHeight: string;
    color: string;
};
export declare const h5Font: {
    fontSize: number;
    lineHeight: string;
    fontWeight: number;
    fontFamily: string[];
    color: string;
};
export declare const h4Font: {
    fontSize: number;
    lineHeight: string;
    fontWeight: number;
    fontFamily: string[];
    color: string;
};
export declare const h3Font: {
    fontSize: number;
    lineHeight: string;
    fontWeight: number;
    fontFamily: string[];
    color: string;
};
export declare const h2Font: {
    fontSize: number;
    lineHeight: string;
    fontWeight: number;
    fontFamily: string[];
    color: string;
};
export declare const h1Font: {
    fontSize: number;
    lineHeight: string;
    fontWeight: number;
    fontFamily: string[];
    color: string;
};
